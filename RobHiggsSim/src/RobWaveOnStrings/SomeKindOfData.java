package RobWaveOnStrings;
/*
 * This interface defines what every wave's data set should do
 * Every wave data should know how to set itself to rest
 */
public interface SomeKindOfData {
	public void setToZero();
}
