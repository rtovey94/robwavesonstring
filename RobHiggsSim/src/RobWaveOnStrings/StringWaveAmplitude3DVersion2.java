package RobWaveOnStrings;
/*
 * This class shows a circular wave in 3D with the ability to rotate it in 2 directions
 * Amplitude is shown as a series of quadratics along the 3D conversion
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.plaf.basic.BasicArrowButton;

@SuppressWarnings("serial")
public class StringWaveAmplitude3DVersion2 extends RealDataWaveDisplayer {
	public Utils3D Factory3D = new Utils3D();
	public double rotationX = 0, rotationY = 0;
	public double lastRotX = 0, lastRotY = 0;
	public JMenuBar menuBar = new JMenuBar();
	public JMenu InputMethod,View,interactionsList;
	public JLabel Type;
	public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	ButtonGroup group = new ButtonGroup();
	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up;
	private BasicArrowButton down;
	
	public StringWaveAmplitude3DVersion2(RealDataWave rsfWaveThing, final OurCosmetics C){	
			super();
			this.waveThing = rsfWaveThing;
			this.C = C;
			View = new JMenu("view");
			Type = new JLabel("type");
			View.add(Type);
			InputMethod = new JMenu("Input method");
			interactionsList = new JMenu("Interactions");
		
			menuBar.add(View);
			menuBar.add(InputMethod);
			menuBar.add(interactionsList);
		
			add(menuBar);

			add(menuBar);
			menuBar.setLocation(5,5);
			menuBar.setVisible(true);
			
			InputMethod.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					setTheIC();
				}
			});
			
			addMouseListener(new MouseListener(){
				@Override
				public void mouseClicked(MouseEvent e) {}
				@Override
				public void mousePressed(MouseEvent e) {
					lastRotY = rotationY;
					lastRotX = rotationX;
				}
				@Override
				public void mouseReleased(MouseEvent e) {
					rotationY = lastRotY-2*(Curr.getX()-Init.getX());
					rotationX = lastRotX-(Curr.getY()-Init.getY());
					rotationY = rotationY%(2*Math.PI);
					rotationX = rotationX%(2*Math.PI);
				}
				@Override
				public void mouseEntered(MouseEvent e) {}
				@Override
				public void mouseExited(MouseEvent e) {}});
			addMouseMotionListener(new MouseMotionListener(){
				@Override
				public void mouseDragged(MouseEvent e) {
					rotationY = lastRotY-2*(Curr.getX()-Init.getX());
					rotationX = lastRotX-(Curr.getY()-Init.getY());
				}
				@Override
				public void mouseMoved(MouseEvent e) {}
			});
			zoomFactor = new JLabel(""+0);
			up = new BasicArrowButton(BasicArrowButton.NORTH);
			down = new BasicArrowButton(BasicArrowButton.SOUTH);
			up.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
					C.basicZoomFactor*=2;
				}
			});
			down.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
					C.basicZoomFactor/=2;
				}
			});
			add(up);
			add(down);
			add(zoomFactor);
			zoomFactor.setLocation(getWidth()-32, 2);
			up.setLocation(getWidth()-42,2);
			up.setSize(10,10);
			down.setLocation(getWidth()-12,2);
			down.setSize(10,10);
			zoomFactor.setSize(20, 10);

	}

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public double specialZoomFactor;
		public boolean doSpecialZoomAboutPositiveMin = false;
		public Color waveNumber;

		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
		public void turnOnSpecialZoomAboutPositiveMin(double factor) {
			doSpecialZoomAboutPositiveMin = true;
			specialZoomFactor = factor;
		}

		public void turnOffSpecialZoomAboutPositiveMin() {
			doSpecialZoomAboutPositiveMin = false;
		}

		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

	@Override
	public void addInitialConditioner(InitialConditioner IC){
	}
	
	@Override
	public boolean setTheIC(){
		return false;
	}

	@Override
	protected void paintComponent(Graphics f) {
		super.paintComponent(f);
		Graphics2D g = (Graphics2D)f;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		double W = (double)getWidth()/4.-10., H = (double)getHeight()/4.-5.;
		
		Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,((RealDataWave)waveThing).getSpaceProps().n);
		double[] vector = Utils3D.cast2D(Utils3D.rotateAboutX(Utils3D.rotateAboutY(new double[]{0, ((OurCosmetics)C).basicZoomFactor* ((RealDataWave)waveThing).data.x[0], 1}, rotationY), rotationX));
		curve.moveTo((2+vector[0])*W, (2-vector[1])*H);
		double deltaAngle = 2 * Math.PI  / ((double) (((RealDataWave)waveThing).getSpaceProps().n));
				
		for (int j = 0; j < ((RealDataWave)waveThing).getSpaceProps().n/2; ++j) {
			double[] vector1 = Utils3D.cast2D(Utils3D.rotateAboutX(Utils3D.rotateAboutY(new double[]{Math.sin((2*j)*deltaAngle), ((OurCosmetics)C).basicZoomFactor* ((RealDataWave)waveThing).data.x[2*j], Math.cos((2*j)*deltaAngle)}, rotationY), rotationX));
			double[] vector2 = Utils3D.cast2D(Utils3D.rotateAboutX(Utils3D.rotateAboutY(new double[]{Math.sin((2*j+1)*deltaAngle), ((OurCosmetics)C).basicZoomFactor* ((RealDataWave)waveThing).data.x[2*j+1], Math.cos((2*j+1)*deltaAngle)}, rotationY), rotationX));
			curve.quadTo((2+vector1[0])*W, (2-vector1[1])*H, (2+vector2[0])*W, (2-vector2[1])*H);
		}
		curve.closePath();
		
		g.setStroke(new BasicStroke(2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(C.getWaveNumber());
		g.draw(curve);

		up.setSize(15,15);
		down.setSize(15,15);
		zoomFactor.setSize(20, 15);
		down.setLocation(getWidth()-17,2);
		zoomFactor.setLocation(getWidth()-37, 2);
		up.setLocation(getWidth()-52,2);
		menuBar.setLocation(2,2);
		menuBar.setSize(menuBar.getPreferredSize());
		menuBar.doLayout();
		g.setStroke(new BasicStroke(1f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(Color.black);
//TODO make this into an oval again when you remember the formula...
		Path2D axis = new Path2D.Float(Path2D.WIND_NON_ZERO,50);
		vector = Utils3D.cast2D(Utils3D.rotateAboutX(Utils3D.rotateAboutY(new double[]{0, 0, 1}, rotationY), rotationX));
		axis.moveTo((2+vector[0])*W, (2-vector[1])*H);
		deltaAngle = 2*Math.PI/100.0;
		for(int j=0;j<51;j++){
			double[] vector1 = Utils3D.cast2D(Utils3D.rotateAboutX(Utils3D.rotateAboutY(new double[]{Math.sin((2*j)*deltaAngle), 0, Math.cos((2*j)*deltaAngle)}, rotationY), rotationX));
			double[] vector2 = Utils3D.cast2D(Utils3D.rotateAboutX(Utils3D.rotateAboutY(new double[]{Math.sin((2*j+1)*deltaAngle), 0, Math.cos((2*j+1)*deltaAngle)}, rotationY), rotationX));
			axis.quadTo((2+vector1[0])*W, (2-vector1[1])*H, (2+vector2[0])*W, (2-vector2[1])*H);
			
		}
		g.draw(axis);
	}
}
