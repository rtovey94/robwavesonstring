package RobWaveOnStrings;
/*
 * This class contains methods that project 3D vectors onto a 2D plane
 * There are also methods to rotate a vector within the 3D plane too
 * The general matrix basis is provided
 */
public class Utils3D {
	public final static double root2 = Math.sqrt(2);
/*		( x )	( 1, 0, 1/root(2))
 * 		( y ) x ( 0, 1, 1/root(2))
 * 		( z )
 */
	public static double[] cast2D(double[] vector){
		if(vector.length!=3){System.out.println("not 3D");return null;}
		double[] tmp = new double[2];
		tmp[0] = vector[0] ;//+ vector[2]*root2/2;
		tmp[1] = vector[1] + vector[2]*root2/2;
		return tmp;
	}
	
/*		( x )	( 1,   0,            0      )
 * 		( y ) x ( 0,  cos(theta), sin(theta))
 * 		( z )	( 0, -sin(theta), cos(theta))
 */
	public static double[] rotateAboutX(double[] vector, double theta){
		if(vector.length!=3){System.out.println("not rotatable");return null;}
		double[] tmp = new double[3];
		tmp[0] = vector[0];
		tmp[1] = vector[1]*Math.cos(theta)+vector[2]*Math.sin(theta);
		tmp[2] = vector[1]*(-Math.sin(theta))+vector[2]*Math.cos(theta);
		return tmp;
	}

/*		( x )	(  cos(theta),   0, sin(theta))
 * 		( y ) x (   0,           1,  0        )
 * 		( z )	( -sin(theta),   0, cos(theta))
 */
	public static double[] rotateAboutY(double[] vector, double theta){
		if(vector.length!=3){System.out.println("not rotatable");return null;}
		double[] tmp = new double[3];
		tmp[0] = vector[0]*Math.cos(theta)+vector[2]*Math.sin(theta);
		tmp[1] = vector[1];
		tmp[2] = vector[0]*(-Math.sin(theta))+vector[2]*Math.cos(theta);
		return tmp;
	}

}
