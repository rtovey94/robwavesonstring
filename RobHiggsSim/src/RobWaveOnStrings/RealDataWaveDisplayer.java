package RobWaveOnStrings;
/*
 * This class represents a particular type of wave displayer guaranteed to have a wave with real data 
 * and a line marking position of the wave
 * TODO try to override the current wave storage type
 */
@SuppressWarnings("serial")
public abstract class RealDataWaveDisplayer extends WaveDisplayer{
	protected double CenterLine = 0;
	
		public RealDataWaveDisplayer(){
			super();
			waveThing = (RealDataWave)waveThing;
		}
}
