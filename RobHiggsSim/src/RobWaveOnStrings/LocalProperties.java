package RobWaveOnStrings;
/*
 * This class is kept very open to try and allow maximum flexibility
 * The only compulsory guarantee is the ability to produce a clone of itself
 */
public abstract class LocalProperties {
	public abstract LocalProperties clone();
}
