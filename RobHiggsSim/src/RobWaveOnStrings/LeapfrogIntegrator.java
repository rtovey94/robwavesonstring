package RobWaveOnStrings;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
/*
 * This is currently the only extension of the main integrator class
 * This integrator calls advance by first advancing V based on X and then advancing X based on these recalculated Vs
 */
public class LeapfrogIntegrator extends Integrator {
//Only deals with LeapfroggableThings
	private Set<LeapfroggableWaveThing> waveThings = new HashSet<LeapfroggableWaveThing>();
	private Set<LeapfroggableInteraction> interactions = new HashSet<LeapfroggableInteraction>();

	private void advanceVBasedOnX(double dt) {
		Iterator<LeapfroggableWaveThing> itW = waveThings.iterator();
		Iterator<LeapfroggableInteraction> itI = interactions.iterator();
		while (itW.hasNext()) {
			itW.next().advanceVBasedOnX(dt);
		}
		while (itI.hasNext()) {
			itI.next().advanceVBasedOnX(dt);
		}
	}

	private void advanceXBasedOnV(double dt) {
		Iterator<LeapfroggableWaveThing> itW = waveThings.iterator();
		while (itW.hasNext()) {
			itW.next().advanceXBasedOnV(dt);
		}
	}

	@Override
	public void advanceAll(double dt) {
		if (dt > 0) {
			advanceVBasedOnX(dt);
			advanceXBasedOnV(dt);
		} else if (dt < 0) {
			advanceXBasedOnV(dt);
			advanceVBasedOnX(dt);
		}
	}

	@Override
	public void add(WaveThing waveThing) {
		try{waveThings.add((LeapfroggableWaveThing)waveThing);}
		catch(ClassCastException e){System.out.println("This Wave is not leapfroggable");}
	}

	@Override
	public void add(IntegratableInteraction interaction) {
		try{interactions.add((LeapfroggableInteraction)interaction);}
		catch(Exception e){System.out.println("This interaction is not leapfroggable");}
	}

	@Override
	public void remove(WaveThing waveThing) {
		waveThings.remove((LeapfroggableWaveThing)waveThing);
	}

	@Override
	public void remove(IntegratableInteraction Interaction) {
		interactions.remove((LeapfroggableInteraction)Interaction);
	}
	
	@Override
	public String toString(){
		return "LeapFrog Integrator";
	}

}
