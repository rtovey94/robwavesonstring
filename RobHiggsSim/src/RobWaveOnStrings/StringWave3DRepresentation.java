package RobWaveOnStrings;
/*
 * This viewer shows both amplitude and velocity on the same 3D graph.
 * Energy is shaded and amplitude is a plain line
 * Sections of line are predicted by quadratics
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.plaf.basic.BasicArrowButton;

@SuppressWarnings("serial")
public class StringWave3DRepresentation extends RealDataWaveDisplayer{
	public OurCosmetics C;
	public JMenuBar menuBar = new JMenuBar();
	public JMenu InputMethod,View,interactionsList;
	public JLabel Type;
	public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	ButtonGroup group = new ButtonGroup();
	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up;
	private BasicArrowButton down;
	
	public StringWave3DRepresentation(RSFWaveThing rsfWaveThing, final OurCosmetics C)
	/*throws Exception{
		if(rsfWaveThing == null){
			throw new Exception(
				"rsfWaveThing null in StringWaveAmplitudeWithCal constructor");
		}
		if(C == null){
			throw new Exception(
				"cosmetics null in StringWaveAmplitudeWithCal constructor");
		}
	//*/{	
			super();
			this.waveThing = rsfWaveThing;
			this.C = C;
			View = new JMenu("view");
			Type = new JLabel("type");
			View.add(Type);
			InputMethod = new JMenu("Input method");
			interactionsList = new JMenu("Interactions");
		
			menuBar.add(View);
			menuBar.add(InputMethod);
			menuBar.add(interactionsList);
		
			add(menuBar);

			add(menuBar);
			menuBar.setLocation(2,2);
			menuBar.setVisible(true);
			
			zoomFactor = new JLabel(""+0);
			up = new BasicArrowButton(BasicArrowButton.NORTH);
			down = new BasicArrowButton(BasicArrowButton.SOUTH);
			up.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
					C.basicZoomFactor1*=2;
					C.basicZoomFactor2*=2;
				}
			});
			down.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
					C.basicZoomFactor1/=2;
					C.basicZoomFactor2/=2;
				}
			});
			add(up);
			add(down);
			add(zoomFactor);
			zoomFactor.setLocation(getWidth()-32, 2);
			up.setLocation(getWidth()-42,2);
			up.setSize(10,10);
			down.setLocation(getWidth()-12,2);
			down.setSize(10,10);
			zoomFactor.setSize(20, 10);
	}

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor1 = 1;
		public double basicZoomFactor2 = 1;
		public double specialZoomFactor;
		public boolean doSpecialZoomAboutPositiveMin = false;
		public Color waveNumber;
		
		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
		public void turnOnSpecialZoomAboutPositiveMin(double factor) {
			doSpecialZoomAboutPositiveMin = true;
			specialZoomFactor = factor;
		}

		public void turnOffSpecialZoomAboutPositiveMin() {
			doSpecialZoomAboutPositiveMin = false;
		}

		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

	@Override
	public void addInitialConditioner(InitialConditioner IC){
		initialConditioners.add(IC);
		Methods.add(new JRadioButtonMenuItem(IC.toString()));
		group.add(Methods.get(Methods.size()-1));
		InputMethod.add(Methods.get(Methods.size()-1));
	}
	
	@Override
	public boolean setTheIC(){
		for(int j=0;j<initialConditioners.size();j++){
			if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
		}
		return true;
	}

	@Override
	protected void paintComponent(Graphics f) {
		super.paintComponent(f);
		Graphics2D g = (Graphics2D)f;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		double W = (double)getWidth(), H = (double)getHeight();
		
		boolean hasVev = true;
		double vev = 0,vevVisualOffset=0;
		try {
			vev = ((RSFWaveThing)waveThing).propRSF.nonNegVev();
			if (vev != 0) {
				vevVisualOffset = ((OurCosmetics)C).basicZoomFactor1 * vev;
			}
		} catch (Exception e) {
			hasVev = false;
		}

		Path2D amplitudeCurve = new Path2D.Float(Path2D.WIND_NON_ZERO,((RealDataWave)waveThing).getSpaceProps().n);
		Path2D energyCurve = new Path2D.Float(Path2D.WIND_EVEN_ODD,2*((RealDataWave)waveThing).getSpaceProps().n);
		amplitudeCurve.moveTo(3.*W/4.0, H/2.0);
		energyCurve.moveTo(3.*W/4.0, H/2.0);
		double deltaAngle = 2 * Math.PI  / ((double) (((RealDataWave)waveThing).getSpaceProps().n));
		double checkNullEnergy=0;
		for (int j = 0; j < ((RealDataWave)waveThing).getSpaceProps().n/2; ++j) {
			double xBase1;
			double yBase1;
			double xBase2;
			double yBase2;
			double X1;
			double E1;
			double X2;
			double E2;
			
			final double angle = 2 * Math.PI * ((double)2*j)/ ((double) (((RealDataWave)waveThing).getSpaceProps().n));
			xBase1 = W / 2.0 + W / 4. * Math.cos(angle);
			yBase1 = H / 2.0 - H / 4. * Math.sin(angle);
			xBase2 = W / 2.0 + W / 4. * Math.cos(angle+deltaAngle);
			yBase2 = H / 2.0 - H / 4. * Math.sin(angle+deltaAngle);
		
			if (hasVev) {
				// double vevOrZero = propRSF.nonNegVevOrZero();
				X1 = yBase1 - ((OurCosmetics)C).basicZoomFactor1 * vev - ((OurCosmetics)C).basicZoomFactor1
						 * (((RealDataWave)waveThing).data.x[2*j] - vev);
				X2 = yBase2 - ((OurCosmetics)C).basicZoomFactor1* vev - ((OurCosmetics)C).basicZoomFactor1
					 * (((RealDataWave)waveThing).data.x[2*j+1] - vev);
				E1 = yBase1 - ((OurCosmetics)C).basicZoomFactor2* vev - ((OurCosmetics)C).basicZoomFactor2
						 * (((Discrete1DEnergyDensity)waveThing).energyAtIndex(2*j) - vev);
				E2 = yBase2 - ((OurCosmetics)C).basicZoomFactor2* vev - ((OurCosmetics)C).basicZoomFactor2
					 * (((Discrete1DEnergyDensity)waveThing).energyAtIndex(2*j+1) - vev);
			} else {
				X1 = yBase1 - ((OurCosmetics)C).basicZoomFactor1* ((RealDataWave)waveThing).data.x[2*j];
				X2 = yBase2 - ((OurCosmetics)C).basicZoomFactor1* ((RealDataWave)waveThing).data.x[2*j+1];
				E1 = yBase1 - ((OurCosmetics)C).basicZoomFactor2* ((Discrete1DEnergyDensity)waveThing).energyAtIndex(2*j);
				E2 = yBase2 - ((OurCosmetics)C).basicZoomFactor2* ((Discrete1DEnergyDensity)waveThing).energyAtIndex(2*j+1);
			}

			if (vev != 0) {
			// TODO turn this into lines
				g.setColor(Color.orange);
				g.fillRect((int) xBase1, (int) (yBase1 + vevVisualOffset), 1, 1);
				g.fillRect((int) xBase1, (int) (yBase1 - vevVisualOffset), 1, 1);
				g.fillRect((int) xBase2, (int) (yBase2 + vevVisualOffset), 1, 1);
				g.fillRect((int) xBase2, (int) (yBase2 - vevVisualOffset), 1, 1);
			}
			amplitudeCurve.quadTo(xBase1, X1, xBase2, X2);
			energyCurve.quadTo(xBase1, E1, xBase2, E2);
			if(checkNullEnergy<E1){checkNullEnergy = E1;}
			if(checkNullEnergy<E2){checkNullEnergy = E2;}

		}
		amplitudeCurve.closePath();

		energyCurve.lineTo(3.*W/4.0, H/2.0);
		for (int j = 0; j < ((RealDataWave)waveThing).getSpaceProps().n/2; ++j) {
			double xBase1;
			double yBase1;
			double xBase2;
			double yBase2;

			
			final double angle = 2 * Math.PI * ((double)2*j)/ ((double) (((RealDataWave)waveThing).getSpaceProps().n));
			xBase1 = W / 2.0 + W / 4. * Math.cos(angle);
			yBase1 = H / 2.0 - H / 4. * Math.sin(angle);
			xBase2 = W / 2.0 + W / 4. * Math.cos(angle+deltaAngle);
			yBase2 = H / 2.0 - H / 4. * Math.sin(angle+deltaAngle);
		
			energyCurve.quadTo(xBase1, yBase1, xBase2, yBase2);
		}
		energyCurve.closePath();
		
		g.setStroke(new BasicStroke(2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(C.waveNumber);
		g.draw(amplitudeCurve);

		g.setColor(new Color(0,0,0,150));
		if(checkNullEnergy>0){g.fill(energyCurve);}
		
		g.setStroke(new BasicStroke(1f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(Color.black);
		g.drawOval((int)W/4, (int)H/4, (int)W/2, (int)H/2);
		
		up.setSize(15,15);
		down.setSize(15,15);
		zoomFactor.setSize(20, 15);
		down.setLocation(getWidth()-17,2);
		zoomFactor.setLocation(getWidth()-37, 2);
		up.setLocation(getWidth()-52,2);
		menuBar.setLocation(2,2);
		menuBar.setSize(menuBar.getPreferredSize());
		menuBar.doLayout();
	}
}
