package RobWaveOnStrings;

import java.util.Hashtable;
/*
 * Here a RSF wave is defined to have real data and has integratable energy
 */
public class RSFWaveThing extends RealDataWave implements LeapfroggableWaveThing,Discrete1DEnergyDensity {
	public RSFProperties propRSF;
	public InitialConditioner theIC;


	public RSFWaveThing(SpaceProperties propSpace, RSFProperties propRSF) {
		super(propSpace);
		this.propRSF = propRSF;
		theIC= new RSFInitialConditioner_PacketType(this,0,0,0,1);
		theIC.setInitialCondition();
	}

	@Override
	public void advanceVBasedOnX(double dt) {
		for(int i = 0; i < data.x.length; ++i) {

			final int nex = (i + 1) % getSpaceProps().n;
			final int pre = (i + getSpaceProps().n - 1) % getSpaceProps().n;

			final double dTwoPhiByDXSquared = (data.x[nex] + data.x[pre] - data.x[i] * 2.0)
					/ (getSpaceProps().delta * getSpaceProps().delta);

			final double phi = data.x[i];

			final double vDot =
					dTwoPhiByDXSquared
					- phi * propRSF.mSq
					- phi * phi * phi * propRSF.quarticTerm;

			data.v[i] += vDot*dt;
		}
		advancePosition(dt);
	}

	@Override
	public void advanceXBasedOnV(double dt) {
		for (int i = 0; i < data.x.length; ++i) {
			data.x[i] += dt * data.v[i];
		}
		data.totalT += dt;
	}
	
//RSF waves are lucky in that they have a calculable velocity through space to help decide position
	@Override
	public void advancePosition(double dt){
		data.position = (data.position +propRSF.velocity*dt)%getSpaceProps().L;
		if(data.position<0){data.position+=getSpaceProps().L;}
	}

	@Override
	public void zeroTheWave() {
		data.setToZero();
	}

	@Override
	public double energyAtIndex(int xi) {
		final int xj = (xi + 1) % getSpaceProps().n;
		final double dPsiByDT = (data.v[xi]);
		final double dPsiByDX = ((data.x[xj] - data.x[xi]) / getSpaceProps().delta);
		final double psi = (data.x[xi]);

		final double ans = (0.5 * dPsiByDT * dPsiByDT + 0.5 * dPsiByDX
				* dPsiByDX + 0.5 * propRSF.mSq * psi * psi + 0.25
				* propRSF.quarticTerm * psi * psi * psi * psi - propRSF
					.isSometimesHeightOfPotMin()) * getSpaceProps().delta;

		return ans;
	}

	public String typeToString(){
		return "Klein Gordon";
	}

	@Override
	public void advance(double dt) {
		advanceVBasedOnX(dt);
		advanceXBasedOnV(dt);
	}

	@Override
	public void setTheIC(InitialConditioner IC) {
		theIC = (RSFInitialConditioner_PacketType) IC;}
	@Override
	public InitialConditioner getTheIC() {
		return theIC;}
	@Override
	public SpaceProperties getSpaceProps() {
		return propSpace;}
	@Override
	public RSFProperties getLocalProps() {
		return propRSF;}
	
	@Override
	public Hashtable<String, String> getLesson1DataSet() {
		Hashtable<String,String> tmp = new Hashtable<String,String>();
		tmp.put("Mass squared", String.format("%3.2f", propRSF.mSq));
		tmp.put("Type", typeToString());
		tmp.put("propRSF.velocity", String.format("%3.2f", propRSF.velocity)+"c");
		tmp.put("Momentum", String.format("%3.2f", Math.sqrt(propRSF.mSq*propRSF.velocity*propRSF.velocity/(1-propRSF.velocity*propRSF.velocity))));
		double tmpE = 0;for(int i=0;i<getSpaceProps().n;i++){tmpE+=energyAtIndex(i);}
		tmp.put("Energy", String.format("%4.3E", tmpE));
		return tmp;
	}
	@Override
	public void setSpaceProps(SpaceProperties SP) {this.propSpace = SP;this.data.resize(SP.n);}
	@Override
	public void setLocalProps(LocalProperties LP) {try{this.propRSF = (RSFProperties)LP;}catch(Exception e){}}

	@Override
	public double getPosition() {
		return data.position;
	}

}
