package RobWaveOnStrings;

public class BOSProperties extends LocalProperties {
//Here are the values necessary to reproduce the initial conditions
	public double K = 0.1;
	public double totalMass = 1;
	public int focusBall;
	public double endPosition;
	public int potential;
	
	
//Sets default properties of the system
	public BOSProperties(){}
	
//Sets the initial properties of the system
	public BOSProperties(double k, double totalMass){
		reset(k,totalMass);
	}
	
	public void reset(double k, double m){
		this.K = k;
		this.totalMass = m;
	}
	
//This returns an identical copy of the current properties
	public BOSProperties clone(){
		BOSProperties tmp = new BOSProperties(K,totalMass);
		tmp.focusBall= this.focusBall;
		tmp.endPosition = this.endPosition;
		tmp.potential = this.potential;
		return tmp;
	}
	
}
