package RobWaveOnStrings;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.event.*;
import java.awt.geom.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;

@SuppressWarnings("serial")
public class Lesson1 extends JPanel{
	public ArrayList<WaveType> WaveTypeList;

	private JPanel WaveContainer;
	private EnergyBar EnergyBar;
	private JPanel Parameters;
	private DataGraph dataGraph;
	private JScrollPane DataTablePane;
	private JButton dataReset;
	private ArrayList<WaveDisplayer> Graph = new ArrayList<WaveDisplayer>();
	private double DT = 0.01;
	private WaveType currentWaveType;
	private Integrator integrator;
	private ActionListener timerAction = new ActionListener(){
		@Override
	   	public void actionPerformed(ActionEvent e) {
			repaint();
			for(int i=0;i<Graph.size();i++){
				if(Graph.get(i).isAdjusting){return;}
			}
			
			double delta = Math.abs(DT);
			if(DT!=0){while(delta<=0.1){integrator.advanceAll(DT);delta+=Math.abs(DT);}integrator.advanceAll((0.1)%DT);addData();}
			
			try{((CalorimeterDisplayer)Graph.get(0)).progressParticles(0.1, Double.parseDouble(EnergyBar.zoomFactor.getText()));
			EnergyBar.energyLevel = (((CalorimeterDisplayer)Graph.get(0)).getEnergyLevel());}catch(ClassCastException f){}
		}
	};
	public Timer myTimer =new Timer(30, timerAction);
	
	
	public Lesson1(ArrayList<WaveType> WTL)throws Exception{
		if(WTL == null){
			throw new Exception("No Waves in Lesson 1");
		}
		setLayout(null);
			this.WaveTypeList = WTL;
			WaveContainer = new JPanel();
			constructWaveContainer();
		EnergyBar = new EnergyBar();
		Parameters = new JPanel(new BorderLayout());
			constructParameters();
		dataGraph = new DataGraph();
		dataGraph.storeButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(((CalorimeterDisplayer)Graph.get(0)).isRecordingWave()){
					((CalorimeterDisplayer)Graph.get(0)).setRecordingWave(false);
					double tmpE = 0;
					try{for(int j=0;j<Graph.get(0).waveThing.getSpaceProps().n;++j){tmpE+=((Discrete1DEnergyDensity)Graph.get(0).waveThing).energyAtIndex(j);}
					dataGraph.EndWave(String.format("%4.3E",tmpE));
					}catch(ClassCastException f){dataGraph.EndWave("N/A");}
					
					addDataSet(Graph.get(0).waveThing.getLesson1DataSet());
				}
			}
		});
			constructDataTable();
		dataReset = new JButton("Reset Data");
			constructDataReset();

		add(WaveContainer);
		add(EnergyBar);
		add(Parameters);
		add(dataGraph);
		add(DataTablePane);
		add(dataReset);
		
		
		setMinimumSize(new Dimension(700,500));
		setBackground(Color.white);
		setOpaque(true);
		setVisible(true);
		myTimer.start();
	}

	private void constructWaveContainer(){
		WaveContainer.setLayout(new GridLayout(0,1));
	
//TODO make this more general
		integrator = new LeapfrogIntegrator();
		reconstructWaves(WaveTypeList.get(0));
		}

	private void reconstructWaves(WaveType waveType){
		SpaceProperties propSpace = new SpaceProperties(Integer.parseInt(propSpaceVariables.parameterValues.get(2).getText()),Double.parseDouble(propSpaceVariables.parameterValues.get(0).getText()));
		try{integrator.remove(Graph.get(0).waveThing);}catch(Exception e){}
		WaveContainer.removeAll();
		Graph.clear();
		currentWaveType = waveType;
		WaveDisplayer[] tmp = currentWaveType.inputDisplays.newLesson1(currentWaveType.createNewWave(propSpace, currentWaveType.LocalProps()),Color.black);
		for(int i=0;i<tmp.length;i++){
			Graph.add(tmp[i]);
			WaveContainer.add(Graph.get(i));
			Graph.get(i).addPropertyChangeListener("refresh", new PropertyChangeListener(){
				@Override
				public void propertyChange(PropertyChangeEvent e){WaveContainer.repaint();}
			});
		}
		integrator.add(Graph.get(0).waveThing);
		WaveContainer.repaint();
	}
	
	private void reconstructWaves(WaveThing waveThing){
		currentWaveType = getWaveType(waveThing.typeToString());
		integrator.remove(Graph.get(0).waveThing);
		WaveContainer.removeAll();
		Graph.clear();
		WaveDisplayer[] tmp = currentWaveType.inputDisplays.newLesson1(waveThing,Color.black);
		for(int i=0;i<tmp.length;i++){
			Graph.add(tmp[i]);
			WaveContainer.add(Graph.get(i));
			Graph.get(i).addPropertyChangeListener("refresh", new PropertyChangeListener(){
				@Override
				public void propertyChange(PropertyChangeEvent e){WaveContainer.repaint();}
			});
		}
		integrator.add(Graph.get(0).waveThing);
		revalidate();
		
	}
	
private Choice WaveTypes;
private JToggleButton pauseWave;
private ControlPanel controlPanel = new ControlPanel();
private SpacePropsControlPanel propSpaceVariables = new SpacePropsControlPanel();
	private void constructParameters(){
		this.WaveTypes = new Choice();
		for(int i=0;i<WaveTypeList.size();i++){
			WaveTypes.add(WaveTypeList.get(i).toString());
		}

		WaveTypes.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				reconstructWaves(getWaveType(WaveTypes.getSelectedItem()));
				reconstructParameterBox();
				currentWaveType.inputDisplays.checkSpacePropsControler(propSpaceVariables);
				invalidate();
			}
		});
		
		WaveTypes.select(0);
		pauseWave = new JToggleButton("Pause Wave");
		pauseWave.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(pauseWave.isSelected()){myTimer.stop();}
				else{myTimer.start();}
			}
		});
		controlPanel.addPropertyChangeListener("refresh and pause", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent e){Graph.get(0).waveThing.getTheIC().setInitialCondition();pauseWave.setSelected(true);myTimer.stop();repaint();
			try{((CalorimeterDisplayer)Graph.get(0)).setNewWave(true);}catch(ClassCastException f){}
			}
		});
		propSpaceVariables.addPropertyChangeListener("refresh and pause", new PropertyChangeListener(){
			 public void propertyChange(PropertyChangeEvent e){
				WaveThing waveThing = currentWaveType.createNewWave(new SpaceProperties(propSpaceVariables.parameterScrolls.get(2).getValue(), propSpaceVariables.parameterScrolls.get(0).getValue())
																	, Graph.get(0).waveThing.getLocalProps());
				waveThing.setTheIC(Graph.get(0).waveThing.getTheIC());
				waveThing.getTheIC().setWaveThing(waveThing);
				reconstructWaves(waveThing);
				reconstructParameterBox();
				try{((CalorimeterDisplayer)Graph.get(0)).setNewWave(true);}catch(ClassCastException f){}
				Graph.get(0).waveThing.getTheIC().setInitialCondition();
				pauseWave.setSelected(true);myTimer.stop();
			 }
		});
		propSpaceVariables.DTPlusMinus.addPropertyChangeListener(new PropertyChangeListener(){
			 public void propertyChange(PropertyChangeEvent e){
				 DT = Double.parseDouble(propSpaceVariables.DTPlusMinus.getText());
				 if(propSpaceVariables.DTPlusMinus.isSelected()){DT*=-1;}repaint();
			 }
		});
		propSpaceVariables.DTPlusMinus.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				 DT = Double.parseDouble(propSpaceVariables.DTPlusMinus.getText());
				 if(propSpaceVariables.DTPlusMinus.isSelected()){DT*=-1;}repaint();
			}
		});
		pauseWave.setSelected(false);
		reconstructParameterBox();
	}
	
	private void reconstructParameterBox(){
		Parameters.removeAll();
		Parameters.add(WaveTypes,BorderLayout.PAGE_START);
		Parameters.add(pauseWave,BorderLayout.PAGE_START);
		currentWaveType.inputDisplays.setInputMethods(controlPanel, Graph.get(0).waveThing);
		Parameters.add(controlPanel, BorderLayout.CENTER);
		Parameters.add(propSpaceVariables, BorderLayout.LINE_END);
	}

private JTable DataTable;
private DataTable data = new DataTable(30);
	private void constructDataTable(){
		DataTable = new JTable(new AbstractTableModel(){
			private String[] columnNames = {"Graph Key", "Wave number", "Type of Wave", "Mass","Speed","Momentum","Energy","Calorimeter Energy", "Potential field", "Other" ,"Repeat Wave"};
			public int getColumnCount(){return data.data[0].length;}
			public int getRowCount(){return data.getRowCount();}
			public Object getValueAt(int row, int col){return data.getValueAt(row,col);}
			public String getColumnName(int c){return columnNames[c];}
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int c){
				return data.getColumnClass(c);
			}
			public boolean isCellEditable(int r, int c){return false;}
			public void setValueAt(Object val, int r, int c){
				data.setValueAt(val,r,c);
				fireTableCellUpdated(r,c);
			}
		});
		DataTable.addMouseListener(new MouseListener(){
			public void mouseEntered(MouseEvent e){}
			public void mouseExited(MouseEvent e){final int n = DataTable.getColumnCount()-2;for(int i=0;i<DataTable.getRowCount();i++){
				if(!DataTable.contains(e.getPoint())){((JMenu)DataTable.getValueAt(i, n)).menuSelectionChanged(false);
						((JPopupMenu)((JMenu)DataTable.getValueAt(i, n)).getPopupMenu()).setVisible(false);}}
			}
			public void mousePressed(MouseEvent e){}
			public void mouseClicked(MouseEvent e){
			int n =((DataTable.getColumnModel()).getColumnIndexAtX(e.getX())); 
			int k = (int)e.getY()/DataTable.getRowHeight();
			if(n==0) {
				if(dataGraph.showLine[k]){
					dataGraph.showLine[k] = false;
					data.setValueAt(new ImageIcon(new byte[]{0}),k,0);
				}else{
					dataGraph.showLine[k] = true;
					data.setValueAt(data.Key[k], k, 0);
				}
			}
			else if(n==DataTable.getColumnCount()-2){
				for(int i=0;i<DataTable.getRowCount();i++){ 
					((JPopupMenu)((JMenu)DataTable.getValueAt(i, n)).getPopupMenu()).setVisible(false);
				}
				Rectangle tmp = DataTable.getCellRect(k, n, false);
				((JPopupMenu)((JMenu)DataTable.getValueAt(k, n)).getPopupMenu()).show(DataTable, tmp.x, tmp.y);;
			}
			else if(n==DataTable.getColumnCount()-1) {
//TODO this does not reproduce the exact wave if you reset it afterwards too
				try{currentWaveType = getWaveType(data.resetButtons[k].waveType);}catch(NullPointerException f){return;}
				WaveTypes.select(data.resetButtons[k].waveType);
				integrator.remove(Graph.get(0).waveThing);
				WaveContainer.removeAll();
				Graph.clear();
				WaveDisplayer[] tmp = currentWaveType.inputDisplays.newLesson1(data.resetButtons[k].theIC.getWaveThing(),Color.black);
				for(int i=0;i<tmp.length;i++){
					Graph.add(tmp[i]);
					WaveContainer.add(Graph.get(i));
				}
				integrator.add(Graph.get(0).waveThing);
				Graph.get(0).waveThing.getTheIC().setInitialCondition();
				reconstructParameterBox();
				revalidate();
			}
		}
		public void mouseReleased(MouseEvent e){}
	});
		DataTable.getColumnModel().getColumn(DataTable.getColumnCount()-2).setCellRenderer(new DefaultTableCellRenderer(){
			public Component getTableCellRendererComponent(JTable table, Object val, boolean isSel, boolean hasFoc, int r, int c){
				JMenu Button = (JMenu)val;
				Button.setOpaque(true);
				if(isSel){
					Button.setForeground(DataTable.getSelectionForeground());
					Button.setBackground(DataTable.getSelectionBackground());
				}else{ 
					Button.setForeground(DataTable.getForeground());
					Button.setBackground(UIManager.getColor("Button.background"));
				}
				return Button;
			}
	});
		DataTable.getColumnModel().getColumn(DataTable.getColumnCount()-1).setCellRenderer(new DefaultTableCellRenderer(){
			public Component getTableCellRendererComponent(JTable table, Object val, boolean isSel, boolean hasFoc, int r, int c){
				JButton Button = (JButton)val;
				Button.setOpaque(true);
				if(isSel){
					Button.setForeground(DataTable.getSelectionForeground());
					Button.setBackground(DataTable.getSelectionBackground());
				}else{ 
					Button.setForeground(DataTable.getForeground());
					Button.setBackground(UIManager.getColor("Button.background"));
				}
				return Button;
			}
	});


	DataTable.getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer(){
			public Component getTableCellRendererComponent(JTable table, Object val, boolean isSel, boolean hasFoc, int r, int c){
				JLabel label = new JLabel((Icon)val);
				return label;
			}
	});

			DataTable.getTableHeader().setReorderingAllowed(false);
			DataTablePane = new JScrollPane(DataTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			DataTable.setAutoResizeMode(4);
			DataTable.setAutoResizeMode(0);
			
		}

	//-----------------------------------------------------------

	private void addData(){
	try{CalorimeterDisplayer Graph0 = (CalorimeterDisplayer)Graph.get(0);
		if(Graph0.isNewWave()){
			Graph0.setRecordingWave(true);
			Graph0.setNewWave(false);
//TODO where should total time be stored?
//TODO who should be able to return total energy?
			dataGraph.StartWave(Graph.get(0).waveThing.getPosition()/Graph.get(0).waveThing.getSpaceProps().L);
		}
		if(Graph0.isRecordingWave()){
			if(((RealDataWave)Graph.get(0).waveThing).data.totalT<30&&((RealDataWave)Graph.get(0).waveThing).data.totalT>=0){dataGraph.addData(((RealDataWave)Graph.get(0).waveThing).data.position/ Graph.get(0).waveThing.getSpaceProps().L,
					((RealDataWave)Graph.get(0).waveThing).data.totalT/ Graph.get(0).waveThing.getSpaceProps().L);
			}else{
				Graph0.setRecordingWave(false);
				double tmpE = 0;
				try{for(int j=0;j<Graph.get(0).waveThing.getSpaceProps().n;++j){tmpE+=((Discrete1DEnergyDensity)Graph.get(0).waveThing).energyAtIndex(j);}
				dataGraph.EndWave(String.format("%4.3E",tmpE));
				}catch(ClassCastException e){dataGraph.EndWave("N/A");}
				
				addDataSet(Graph.get(0).waveThing.getLesson1DataSet());
			}
		}
	}catch(ClassCastException e){return;}
	}

	//-----------------------------------------------------------

		private void addDataSet(Hashtable<String,String> InitialConditions){
			int WaveN = dataGraph.graphN-1;
			if(WaveN<DataTable.getRowCount()){		
				DataTable.setValueAt(new Integer(WaveN+1),WaveN,1);
				if(InitialConditions.get("Type")!=null){DataTable.setValueAt(InitialConditions.get("Type"),WaveN,2);InitialConditions.remove("Type");}else{DataTable.setValueAt("N/A",WaveN,2);}
				if(InitialConditions.get("Mass")!=null){DataTable.setValueAt(InitialConditions.get("Mass"),WaveN,3);InitialConditions.remove("Mass");}else{DataTable.setValueAt("N/A",WaveN,3);}
				if(InitialConditions.get("Velocity")!=null){DataTable.setValueAt(InitialConditions.get("Velocity"),WaveN,4);InitialConditions.remove("Velocity");}else{DataTable.setValueAt("N/A",WaveN,4);}
				if(InitialConditions.get("Momentum")!=null){DataTable.setValueAt(InitialConditions.get("Momentum"),WaveN,5);InitialConditions.remove("Momentum");}else{DataTable.setValueAt("N/A",WaveN,5);}
				if(InitialConditions.get("Energy")!=null){DataTable.setValueAt(InitialConditions.get("Energy"),WaveN,6);InitialConditions.remove("Energy");}else{DataTable.setValueAt("N/A",WaveN,6);}
				try{DataTable.setValueAt(""+(((CalorimeterDisplayer)Graph.get(0)).getEnergyLevel()), WaveN, 7);}catch(ClassCastException f){DataTable.setValueAt("N/A", WaveN, 7);}
				if(InitialConditions.get("Potential Field")!=null){DataTable.setValueAt(""+InitialConditions.get("Potential Field"),WaveN,8);InitialConditions.remove("Potential Field");}else{DataTable.setValueAt("N/A",WaveN,8);}
				if(InitialConditions.size()>0){
					JMenu tmp = new JMenu("Expand");
					Iterator<Entry<String, String>> it1 = InitialConditions.entrySet().iterator();
					Iterator<Entry<String, String>> it2 = InitialConditions.entrySet().iterator();
					while(it1.hasNext()){
						tmp.add(new JLabel(it1.next().getKey()+" = "+ it2.next().getValue()));
					}
					data.setValueAt(tmp,WaveN,DataTable.getColumnCount()-2);}
				else{DataTable.setValueAt(new JMenu("N/A"), WaveN,8);}
				SpaceProperties tmp1 = Graph.get(0).waveThing.getSpaceProps().clone();
				LocalProperties tmp2 = Graph.get(0).waveThing.getLocalProps().clone();
				Object[] tmp = {currentWaveType.toString(), tmp1, tmp2, Graph.get(0).waveThing.getTheIC()}; 
				DataTable.setValueAt(tmp,WaveN,DataTable.getColumnCount()-1);
			}
		}

	//-----------------------------------------------------------

		private void constructDataReset(){
			dataReset.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					data = new DataTable(data.getRowCount());
					dataGraph.graphN = 0;
					dataGraph.Line = new Path2D[30];
				}
			});
		}


		//-----------------------------------------------------------

		public void resetPanels(){
			int H = getHeight();
			int W = getWidth();

			WaveContainer.setSize(W-80,H-250);
			EnergyBar.setSize(80,H-250);
			Parameters.setSize(400,250);
			dataGraph.setSize(250,250);
			DataTablePane.setSize(W-650,224);
			dataReset.setSize(DataTablePane.getWidth(),26);

			WaveContainer.setLocation(0,0);
			EnergyBar.setLocation(W-80,0);
			Parameters.setLocation(0,H-250);
			dataGraph.setLocation(400,H-250);
			DataTablePane.setLocation(650,H-250);
			dataReset.setLocation(650, H-26);

			EnergyBar.setBackground(Color.orange);
			Parameters.setBackground(Color.white);
			dataGraph.setBackground(Color.white);
			DataTablePane.setBackground(Color.white);

			DataTable.setFillsViewportHeight(true);
			DataTable.setPreferredScrollableViewportSize(DataTable.getPreferredSize());

		}

		 private WaveType getWaveType(String type) {
				for(int j=0; j<WaveTypeList.size();j++){
					if(WaveTypeList.get(j).toString()==type){
						return WaveTypeList.get(j);}
				}
			System.out.println("getWaveType(string) has returned null");
			 return null;
		}

}
