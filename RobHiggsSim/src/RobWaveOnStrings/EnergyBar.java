package RobWaveOnStrings;

import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.*;
import java.awt.*;
/*
 * This object shows the energy bar in the top right of lesson 1
 * The user dictates the zoom factor and the bar takes that as its maximum
 */
@SuppressWarnings("serial")
public class EnergyBar extends JPanel{
	public double energyLevel = 0;
	public JLabel EnergyLabel,zoomFactor;

	//-----------------------------------------------------------
		public EnergyBar(){
			this.EnergyLabel = new JLabel("Energy");
			add(EnergyLabel);
			EnergyLabel.setLocation(15,5);
			EnergyLabel.setSize(60,20);
			setLayout(null);
			zoomFactor = new JLabel(String.format("%1.0E",1.0));
			JButton up = new BasicArrowButton(BasicArrowButton.NORTH);
			JButton down = new BasicArrowButton(BasicArrowButton.SOUTH);
			up.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(String.format("%1.0E",Double.parseDouble(zoomFactor.getText())*10));
				}
			});
			down.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(String.format("%1.0E",Double.parseDouble(zoomFactor.getText())/10));
				}
			});
			add(up);
			add(down);
			add(zoomFactor);
			up.setLocation(0,25);
			down.setLocation(60, 25);
			zoomFactor.setLocation(20, 25);
			up.setSize(20, 19);
			down.setSize(20,19);
			zoomFactor.setSize(40, 19);
		}

	//-----------------------------------------------------------

		@Override
		protected void paintComponent(Graphics f) {
			super.paintComponent(f);
			Graphics2D g = (Graphics2D)f;

			g.setStroke(new BasicStroke(2f));
			g.setColor(Color.black);
			double H = getHeight()-55;
			g.draw(new Rectangle2D.Double(20,46, 25,H));
			for(int i=1;i<11;i++){
				int h = (int)Math.round(i*H/10);
				g.drawLine(20,45+h,45, 45 + h);
				g.drawString(String.format("%2.1f",0.1*(10-i)),48,45+h+5);
			}

			g.setColor(Color.red);
			double zoomfactorN = Double.parseDouble(zoomFactor.getText());
			if(energyLevel>zoomfactorN){g.fillRect(21, 46, 23, (int)H);}
			else{g.fillRect(20+1,40+(int)Math.round(H-H*energyLevel/(zoomfactorN))+6 , 25-2,(int)Math.round(H*energyLevel/(zoomfactorN))-1);}

		}
}
