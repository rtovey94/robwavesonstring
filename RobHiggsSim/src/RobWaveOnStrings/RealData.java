package RobWaveOnStrings;

import java.util.Arrays;
/*
 * The real data class is a set of storage points for a wave where its data can be represented purely in real values
 * This X, V data is stored in arrays
 */
public class RealData implements SomeKindOfXVData {
	public double[] x;
	public double[] v;
	public double position,totalT;

	public RealData(int n) {
		x = new double[n];
		v = new double[n];
		position = 0;
		totalT = 0;
	}
	
	public void resize(int n){
		if(n!=x.length){x = new double[n]; v = new double[n];}

	}

	@Override
	public void setXToZero() {
		Arrays.fill(x, 0);
		position = 0;
	}

	@Override
	public void setVToZero() {
		Arrays.fill(v, 0);
	}

	@Override
	public void setToZero() {
		setXToZero();
		setVToZero();
		totalT = 0;
	}
}
