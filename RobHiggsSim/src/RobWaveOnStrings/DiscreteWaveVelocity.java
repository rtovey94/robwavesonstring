package RobWaveOnStrings;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.plaf.basic.BasicArrowButton;
/*
 * This display shows dot-to-dot style velocity against position
 * TODO possibly show it as bar chart
 */
@SuppressWarnings("serial")
public class DiscreteWaveVelocity extends RealDataWaveDisplayer{
	private JMenuBar menuBar = new JMenuBar();
	public JMenu InputMethod,View,interactionsList;
	public JLabel Type;
	public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	private ButtonGroup group = new ButtonGroup();
	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up, down;

	public DiscreteWaveVelocity(RealDataWave rsfWaveThing, final OurCosmetics C) {	
		super();
		this.waveThing = rsfWaveThing;
		this.C = C;
		View = new JMenu("view");
		Type = new JLabel("type");
		View.add(Type);
		InputMethod = new JMenu("Input method");
		interactionsList = new JMenu("Interactions");
	
		menuBar.add(View);
		menuBar.add(InputMethod);
		menuBar.add(interactionsList);
	
		add(menuBar);

		add(menuBar);
		menuBar.setLocation(5,5);
		menuBar.setVisible(true);
		
		zoomFactor = new JLabel(""+0);
		up = new BasicArrowButton(BasicArrowButton.NORTH);
		down = new BasicArrowButton(BasicArrowButton.SOUTH);
		up.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
				C.basicZoomFactor*=2;
			}
		});
		down.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
				C.basicZoomFactor/=2;
			}
		});
		add(up);
		add(down);
		add(zoomFactor);
		zoomFactor.setLocation(getWidth()-32, 2);
		up.setLocation(getWidth()-42,2);
		up.setSize(10,10);
		down.setLocation(getWidth()-12,2);
		down.setSize(10,10);
		zoomFactor.setSize(20, 10);


	}

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public Color waveNumber;
		
		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

	@Override
	public void addInitialConditioner(InitialConditioner IC){
		initialConditioners.add(IC);
		Methods.add(new JRadioButtonMenuItem(IC.toString()));
		group.add(Methods.get(Methods.size()-1));
		InputMethod.add(Methods.get(Methods.size()-1));
	}
	
	@Override
	public boolean setTheIC(){
		for(int j=0;j<initialConditioners.size();j++){
			if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
		}
		return true;
	}

	@Override
	protected void paintComponent(Graphics f) {
		super.paintComponent(f);
		Graphics2D g = (Graphics2D)f;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.setStroke(new BasicStroke(3f));
		g.drawLine(20,25, 20,getHeight()-15);
		g.drawLine(20,getHeight()/2+5, getWidth()-20, getHeight()/2+5);


		int dataLength = ((RealDataWave)waveThing).data.x.length;
		double xScale;
		if((dataLength%2)==0){xScale = (getWidth()-40)/(dataLength-1.0);}
		else{xScale = (getWidth()-40)/(dataLength-1.0);}

		g.setColor(Color.red);
		CenterLine = ((RealDataWave)waveThing).data.position/ ((RealDataWave)waveThing).getSpaceProps().delta -0.5;
		if(CenterLine>0){
			g.drawLine(20+(int)(CenterLine*xScale),25, 20+(int)(CenterLine*xScale),getHeight()-15);

		}


		double yScale = (getHeight()-40)/2.*((OurCosmetics)C).basicZoomFactor;

		Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,dataLength);
		curve.moveTo(20, getHeight()/2-((RealDataWave)waveThing).data.v[0]*yScale+5);

		double width = (getWidth()-40.0)/(dataLength-1.)/2.;
		g.setColor(C.getWaveNumber());
		for(int i=0; i<dataLength; i++){
			curve.lineTo(20+i*xScale,getHeight()/2-((RealDataWave)waveThing).data.v[i]*yScale+5);
			g.fillOval(20+(int)(i*xScale-width/2.),(int)(5+getHeight()/2-((RealDataWave)waveThing).data.v[i]*yScale-width/2.), (int)width, (int)width);
		}


		g.setStroke(new BasicStroke(1f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.draw(curve);

		up.setSize(15,15);
		down.setSize(15,15);
		zoomFactor.setSize(20, 15);
		down.setLocation(getWidth()-17,2);
		zoomFactor.setLocation(getWidth()-37, 2);
		up.setLocation(getWidth()-52,2);
		menuBar.setLocation(2,2);
		menuBar.setSize(menuBar.getPreferredSize());
		menuBar.doLayout();
		g.setStroke(new BasicStroke(1f));
		g.setColor(Color.black);
	}

}
