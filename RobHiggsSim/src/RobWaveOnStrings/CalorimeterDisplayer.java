package RobWaveOnStrings;

public interface CalorimeterDisplayer{
/*
 * The main requirements of a calorimeter displayer can mainly be shown by getters and setters 
 * There is also the method of progressing the particles in the calorimeter
 */
	public boolean isNewWave();
	public boolean isRecordingWave();
	public double getEnergyLevel();
	public void setNewWave(boolean b);
	public void setRecordingWave(boolean b);
	public void progressParticles(double dt, double MaxE);

}
