package RobWaveOnStrings;

public class BOSInitialConditioner_Plucker extends BOSInitialConditioner{
	/*
	 * Here is the main user interface, it allows users to "drag" a ball and release it
	 * It rounds the initial mouse point to the closest rest ball and then drags from there
	 */
//It keeps track of the WaveDisplayer so that it knows where to rip the correct values from
	private RealDataWaveDisplayer waveDisplayer;
	
		public BOSInitialConditioner_Plucker(RealDataWaveDisplayer WD){
			super((BOSWaveThing) WD.waveThing, 1,0);
			this.waveDisplayer = WD;
		}

		public void resetWave(){
			bosWaveThing.propBOS.focusBall = (int)Math.round(bosWaveThing.getSpaceProps().n*waveDisplayer.Init.getX());
			if(bosWaveThing.propBOS.focusBall==0){bosWaveThing.propBOS.focusBall+=1;}
			else if(bosWaveThing.propBOS.focusBall==bosWaveThing.getSpaceProps().n-1){bosWaveThing.propBOS.focusBall-=1;}
			bosWaveThing.propBOS.endPosition = waveDisplayer.Curr.getX();
			resetConditions(bosWaveThing.propBOS.focusBall, bosWaveThing.propBOS.endPosition);
		}
		
		@Override
		public void setInitialCondition(){
			bosWaveThing.data.setToZero();
			resetWave();
			putPacket();
		}
		
		@Override
		public String toString(){
			return "Pluck";
		}
}
