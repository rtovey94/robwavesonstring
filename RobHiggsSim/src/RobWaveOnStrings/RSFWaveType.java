package RobWaveOnStrings;

public class RSFWaveType extends WaveType{
/*
 * The RSF wave type can represent any real scalar field wave
 * The evolution of this wave is calculated through its Lagrangian
 * Integration methods are provided for leap-frog only at the moment
 */
	public RSFWaveType(){
		super();
		this.inputDisplays = new RSFInterfaceList();
	}
	
	@Override
	public RSFWaveThing createNewWave(SpaceProperties propSpace,
			LocalProperties localProp) {
			propSpace.setCircular(true);
			RSFProperties rsfp;
			try{rsfp = (RSFProperties)localProp;}catch(Exception e){
				System.out.println("createNewWave() has returned null in RSF");
				return null;}
		return new RSFWaveThing(propSpace, rsfp);
	}

	@Override
	public String toString() {
		return "Klein Gordon";
	}
	
	@Override
	public RSFProperties LocalProps(){
		return new RSFProperties();
	}

}
