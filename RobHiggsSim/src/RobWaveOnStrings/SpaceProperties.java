package RobWaveOnStrings;
/*
 * These are the most general properties applicable to all waves
 */
public class SpaceProperties {
	public int n;
	public double piOnN;
	public double L;
	public double delta;
	public double maxMom;
	public double dp;
	public double oneOverDp;
	public boolean isCircular, restrictedView= false;

	SpaceProperties(int n, double L) {
		reset(n, L);
	}

	void reset(int n, double L) {
		this.n = n;
		this.L = L;
		this.piOnN = Math.PI / n;
		this.maxMom = Math.PI * 2.0 * n / L;
		this.dp = Math.PI * 2.0 / L;
		this.oneOverDp = 1.0 / (this.dp);
	}

//This method dictates whether the system is circular or if it has ends which aren't connected
//delta will not be initiated until this is set (delta is the physical space between each discretisation of the wave)
	void setCircular(boolean isCircular){
		this.isCircular = isCircular;
		if(isCircular){this.delta = L / (double)n;}
		else{this.delta = L/(n-1.0);}
	}

	public void display() {
		System.out.println("Max Mom is " + this.maxMom);
	}
	
	public SpaceProperties clone(){
		return new SpaceProperties(n,L);
	}
}
