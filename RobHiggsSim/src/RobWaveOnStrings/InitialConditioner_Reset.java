package RobWaveOnStrings;

public class InitialConditioner_Reset implements InitialConditioner{
private WaveThing waveThing;
	public InitialConditioner_Reset(WaveThing wt){
		this.waveThing = wt;
	}
	
	@Override
	public void setInitialCondition() {
		waveThing.getTheIC().setInitialCondition();
	}

	@Override
	public WaveThing getWaveThing() {
		return waveThing;
	}

	@Override
	public void setWaveThing(WaveThing wt) {
		this.waveThing = wt;
	}
	
	@Override
	public String toString(){
		return "Reset";
	}

}
