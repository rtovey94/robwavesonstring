package RobWaveOnStrings;
/*
 * Here the definition of a Leapfroggable wave is something that can advance X and V independently 
 */
public interface LeapfroggableWaveThing extends IntegratableThing {
	public void advanceVBasedOnX(double dt);
	public void advanceXBasedOnV(double dt);
}
