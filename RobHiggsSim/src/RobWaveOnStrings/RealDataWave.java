package RobWaveOnStrings;
/*
 * A real data wave is a wave which guarantees using a real data set
 */
public abstract class RealDataWave implements WaveThing{
	protected SpaceProperties propSpace;
	protected final RealData data;
	
		public RealDataWave(SpaceProperties propSpace){
				this.propSpace = propSpace;
				this.data = new RealData(propSpace.n);
		}
}
