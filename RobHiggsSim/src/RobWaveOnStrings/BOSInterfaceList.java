package RobWaveOnStrings;

import java.awt.Color;

public class BOSInterfaceList extends UIList {
/*
 * This class stores details for all viewers and initial conditioners and interactions associated with this wave type
 */
//Here are the translations of which integers refer to which displays
	public final static int BOS_AmplitudeWithCal = 0;
	public final static int BOS_Amplitude = 1;
	public final static int BOS_Velocity = 2;
	public final static int BOS_Energy = 3;
//Here are the lists of displays and interaction available to general waves
	public final static String[] Display = {"Amplitude", "Velocity", "Energy"};
	public final static String[] Interactions = {"AB", "AABB", "ABC"};

	@Override
	public String[] getDisplayTypes() {
		return Display;
	}
	
//The menu in lesson 2 should have full access to all interactions available
//The pop-up menu should only show binary binding interactions
	@Override
	public String[] getInteractions(boolean fullList){
		if(fullList){return new String[]{"AB","AABB","ABC"};}
		else{return new String[]{"AB", "AABB"};}
	}

	@Override
	public WaveDisplayer newDisplay(WaveThing wt, Color C, int i) {
//This method tries to convert a coloured wave and int into the right wave viewer
//Then calls setInputMethods at the end to give them some initial conditioners
//Returns a null displayer if the integer is out of bounds or the wave cannot be cast
		RealDataWaveDisplayer WD = null;
		BOSWaveThing WT;
		try{WT = (BOSWaveThing)wt;
		switch(i){
		case 0: WD = new BOSWaveAmplitudeWithCal(WT, new BOSWaveAmplitudeWithCal.OurCosmetics(C));
				((BOSWaveAmplitudeWithCal)WD).View.setText(Display[i]);
				((BOSWaveAmplitudeWithCal)WD).Type.setText("Balls on Springs");
				break;
		case 1:	WD = new BOSWaveAmplitude(WT, new BOSWaveAmplitude.OurCosmetics(C));
				((BOSWaveAmplitude)WD).View.setText(Display[i-1]);
				((BOSWaveAmplitude)WD).Type.setText("Balls on Springs");
				break;
		case 2:	WD = new DiscreteWaveVelocity(WT, new DiscreteWaveVelocity.OurCosmetics(C));
				((DiscreteWaveVelocity)WD).View.setText(Display[i-1]);
				((DiscreteWaveVelocity)WD).Type.setText("Balls on Springs");	
				break;
		case 3: WD = new DiscreteWaveEnergy(WT, new DiscreteWaveEnergy.OurCosmetics(C));
				((DiscreteWaveEnergy)WD).View.setText(Display[i-1]);
				((DiscreteWaveEnergy)WD).Type.setText("Balls on Springs");
				break;
		}
		if(i>3){System.out.println("newDisplay() has returned null in BOS UI");}
			setInputMethods(WD, i);
			return WD;
			}catch(ClassCastException e){
				System.out.println("newDisplay() has returned null in BOS UI"+e.toString());
				return null;}
	}

//Returns a calorimeter-amplitude graph, an energy graph and a 3D viewer for amplitude and energy
	@Override
	public WaveDisplayer[] newLesson1(WaveThing wt, Color C){
		return new WaveDisplayer[]{newDisplay(wt,C,0),newDisplay(wt,C,3)};
	}
	
//Only the amplitude displays have initial conditioners, each gets a quick default and an interactive one
	@Override
	public void setInputMethods(WaveDisplayer wd, int i) {
		RealDataWaveDisplayer WD;
		try{WD = (RealDataWaveDisplayer)wd;}catch(ClassCastException e){return;}
		switch(i){
		case 0:	
			WD.addInitialConditioner(new BOSInitialConditioner((BOSWaveThing) WD.waveThing,5,0.5));
			WD.addInitialConditioner(new BOSInitialConditioner_Plucker(WD));
			((BOSWaveAmplitudeWithCal)WD).Methods.get(1).setSelected(true);
			break;
		case 1:	
			WD.addInitialConditioner(new BOSInitialConditioner((BOSWaveThing) WD.waveThing,5,0.5));
			WD.addInitialConditioner(new BOSInitialConditioner_Plucker(WD));
			((BOSWaveAmplitude)WD).Methods.get(1).setSelected(true);
			break;
		}
	}

//Here is the custom control panel for a BOS wave
//From here you can decide on the mass of the system, which ball to pull and how far to pull it, if there's any potential energy and Hooke's coefficient
	@Override
	public void setInputMethods(ControlPanel CP, WaveThing wt) {
		BOSWaveThing WT;
		try{WT = (BOSWaveThing)wt;
		String[] paramNames = {"Mass", "Select Ball","Select displacement", "Potential Energy", "Hooke's constant"};
		int[] lowerBound =       { 1,   2,               -50,  0,  1};
		int[] initial =          { ((int)WT.propBOS.totalMass*10),  WT.propBOS.focusBall, 0,   0,  ((int)WT.propBOS.K*100)};
		int[] upperBound =       { 50,  WT.getSpaceProps().n-1, 50,  10, 20};
		final double[] scale =   { 0.1, 1,                0.1, 1,  0.01};
		CP.initiateScrollBars(paramNames, lowerBound, initial, upperBound, scale);
		CP.TheIC = new BOSInitialConditioner_ControlPanel(CP, WT);
		}catch(Exception e){
			System.out.println("setInputMethods() returned null in BOS");
		}

	}

//this pulls back the initial space conditions to n=10, L = 30 if they are set widely out of range
	@Override
	public void checkSpacePropsControler(SpacePropsControlPanel SPCP){
		if(SPCP.parameterScrolls.get(0).getValue()<10||SPCP.parameterScrolls.get(0).getValue()>60){SPCP.parameterScrolls.get(0).setValue(30);}
		if(SPCP.parameterScrolls.get(2).getValue()>20){SPCP.parameterScrolls.get(2).setValue(10);}
	}

//Here are the beginnings of a return interaction system, hopefully it won't need to be changed much
	@Override
	public IntegratableThing addInteraction(WaveThing[] wave, int i) {
		try{
			if(i==0){return (IntegratableThing) new BOSInteractionAB((BOSWaveThing)wave[0], (BOSWaveThing)wave[1], 1);}
			else if(i==1){return (IntegratableThing) new BOSInteractionAABB((BOSWaveThing)wave[0], (BOSWaveThing)wave[1], 1);}
			else if(i==2){return (IntegratableThing) new BOSInteractionABC((BOSWaveThing)wave[0], (BOSWaveThing)wave[1], (BOSWaveThing)wave[2], 1);}
			else{System.out.println("addInteraction() returned null in BOSInterface"); return null;}
		}catch(Exception e){System.out.println("addInteraction() returned null in BOSInterface");
		return null;}
		
	}

}
