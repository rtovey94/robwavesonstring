package RobWaveOnStrings;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Point2D;

import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
/*
 * This is the most primitive class of wave viewer
 * It sets out that every displayer has the same sorts of aesthetic feel, mouse interface variables and interaction pop-up menu
 */
@SuppressWarnings("serial")
public abstract class WaveDisplayer extends JPanel {
	protected boolean isAdjusting = false, hasFocus = false;
	//Contains data concerning start, middle and end points of mouse along with an approximate velocity
	//All points will return values -1-->1
	protected Point2D Init, Curr, Fin, Vel;
	protected double samples;
	protected Point lastSource;
	protected long lastTime;
	protected WaveThing waveThing;
	public JPopupMenu InteractionMenu = new JPopupMenu(){
		@Override
		public void paintComponent(Graphics g){
			InteractionMenu.getParent().setSize(InteractionMenu.getParent().getPreferredSize());
			InteractionMenu.setSize(InteractionMenu.getPreferredSize());
		}
	};
	public JMenu Interaction = new JMenu("add Interaction");
	protected GlobalCosmetics C;
	public WaveDisplayer(){
		InteractionMenu.add(Interaction);
		Init = new Point2D.Double();
		Fin = new Point2D.Double();
		Vel = new Point2D.Double();
		Curr = new Point2D.Double();
		setLayout(null);
		setBackground(Color.white);
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLoweredBevelBorder(), BorderFactory.createRaisedBevelBorder()));
		setOpaque(true);
		setVisible(true);
		setMinimumSize(new Dimension(300,100));

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if(!(new Rectangle(20,10,getWidth()-40,getHeight()-20)).contains(e.getPoint())){return;}
//needs some work done on it but does give a velocity of sorts
				samples+=1;
				Point source = e.getPoint();
				double H = getHeight()/2.;
				double xL = getWidth()-40;
				double yL = 0.5*(getHeight() - 40);
				Curr.setLocation((source.x-20)/xL,(H-source.y)/yL);
				Vel.setLocation((samples-1)*Vel.getX()/samples + ((double)(source.x-lastSource.x))/((double)(System.nanoTime()-lastTime))/samples*111111, (samples-1)*Vel.getY()/samples + ((double)(source.y-lastSource.y))/((double)(System.nanoTime()-lastTime))/samples*250000);
				lastSource = source;
				lastTime = System.nanoTime();
				if(setTheIC()){try{waveThing.getTheIC().setInitialCondition();
				firePropertyChange("refresh",true,false);}catch(Exception f){}}
			}
			@Override public void mouseMoved(MouseEvent e) {}
		});
		addMouseListener(new MouseAdapter() {
			@Override public void mouseClicked(MouseEvent e) {}
			@Override public void mousePressed(MouseEvent e) {
				requestFocus();
				if(!(new Rectangle(20,10,getWidth()-40,getHeight()-20)).contains(e.getPoint())){return;}
				isAdjusting = true;
				samples = 0;
				lastSource = e.getPoint();
				lastTime = System.nanoTime();
				Vel = new Point2D.Double();
				double H = getHeight()/2.;
				double xL = getWidth()-40;
				double yL = 0.5*(getHeight() - 40);
				Point source = e.getPoint();
				Init.setLocation((source.x-20)/xL,(H-source.y)/yL);
				Curr.setLocation((source.x-20)/xL,(H-source.y)/yL);
				if(setTheIC()){try{waveThing.getTheIC().setInitialCondition();
				firePropertyChange("refresh",true,false);}catch(Exception f){}}
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				isAdjusting = false;
				if(!(new Rectangle(20,10,getWidth()-40,getHeight()-20)).contains(e.getPoint())){
					if(waveThing.getTheIC()!=null){waveThing.getTheIC().setInitialCondition();}
					firePropertyChange("refresh",true,false);return;}
				double H = getHeight()/2.;
				double xL = getWidth()-40;
				double yL = 0.5*(getHeight() - 40);
				Point source = e.getPoint();
				Curr.setLocation((source.x-20)/xL,(H-source.y)/yL);
				Fin.setLocation((source.x-20)/xL,(H-source.y)/yL);
				if(setTheIC()){try{waveThing.getTheIC().setInitialCondition();
				firePropertyChange("refresh",true,false);}catch(Exception f){}}
			}
		});
	}
	
	public abstract boolean setTheIC();
	public abstract void addInitialConditioner(InitialConditioner IC);
	public void setFocus(boolean b){
		if(b){setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.red), BorderFactory.createLineBorder(Color.red)));
		}else{setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLoweredBevelBorder(), BorderFactory.createRaisedBevelBorder()));}
		hasFocus = b;
	}
	
}
