package RobWaveOnStrings;

import java.awt.Color;
/*
 * This is the most general form of cosmetics class, all local cosmetics classes extend from this
 */
public abstract class GlobalCosmetics {
//Currently the only cosmetic property common to all displayers is a colour to display the wave with
	public abstract Color getWaveNumber();

}
