package RobWaveOnStrings;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.plaf.basic.BasicArrowButton;

@SuppressWarnings("serial")
public class BOSWaveAmplitudeWithCal extends RealDataWaveDisplayer implements CalorimeterDisplayer{
	/*
	 * This is the BOS displayer which serves as the calorimeter displayer in lesson 1
	 * It displays position of the balls and springs and produces a calorimeter effect for users 
	 */
//These are the the storing points for things specific to being a calorimeterDisplayer
	private boolean newWave,recordingWave = false;
	private double EnergyLevel;
	private double[] PX,PY,VX,VY;
	private double totalVSq = 0;

//These are the things principal to user interaction with the display	
	private JMenuBar menuBar = new JMenuBar();
	JMenu InputMethod,View,interactionsList;
	JLabel Type;
	ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	private ButtonGroup group = new ButtonGroup();
	private ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up, down;
	
	public BOSWaveAmplitudeWithCal(RealDataWave waveThing, final OurCosmetics C){
		super();
		this.waveThing = waveThing;
		this.C = C;
		View = new JMenu("view");
		Type = new JLabel("type");
		View.add(Type);
		InputMethod = new JMenu("Input method");
		interactionsList = new JMenu("Interactions");
	
//Positions are given random values between 0-10 while Velocities are distributed according to the Boltzmann gas energy distributions
//TODO check what I actually did...
		PX = new double[25];
		PY = new double[25];
		for(int i=0;i<25;i++){PX[i] = (10*Math.random());PY[i] = (10*Math.random());}
		VX = new double[25];
		VY = new double[25];
		double rand = 2*Math.PI*Math.random();VX[0] = Math.cos(rand);VY[0] = Math.sin(rand);
		rand = 2*Math.PI*Math.random();VX[1] = (1.3*Math.cos(rand));VY[1] = (1.3*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[2] = (1.6*Math.cos(rand));VY[2] = (1.6*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[3] = (1.9*Math.cos(rand));VY[3] = (1.9*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[4] = (1.9*Math.cos(rand));VY[4] = (1.9*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[5] = (2.2*Math.cos(rand));VY[5] = (2.2*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[6] = (2.2*Math.cos(rand));VY[6] = (2.2*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[7] = (2.5*Math.cos(rand));VY[7] = (2.5*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[8] = (2.5*Math.cos(rand));VY[8] = (2.5*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[9] = (2.5*Math.cos(rand));VY[9] = (2.5*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[10] = (2.8*Math.cos(rand));VY[10] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[11] = (2.8*Math.cos(rand));VY[11] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[12] = (2.8*Math.cos(rand));VY[12] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[13] = (2.8*Math.cos(rand));VY[13] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[14] = (3.1*Math.cos(rand));VY[14] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[15] = (3.1*Math.cos(rand));VY[15] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[16] = (3.1*Math.cos(rand));VY[16] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[17] = (3.1*Math.cos(rand));VY[17] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[18] = (3.1*Math.cos(rand));VY[18] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[19] = (3.4*Math.cos(rand));VY[19] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[20] = (3.4*Math.cos(rand));VY[20] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[21] = (3.4*Math.cos(rand));VY[21] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[22] = (3.4*Math.cos(rand));VY[22] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[23] = (3.4*Math.cos(rand));VY[23] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[24] = (3.4*Math.cos(rand));VY[24] = (3.4*Math.sin(rand));
		for(int i=0;i<25;i++){totalVSq+=VX[i]*VX[i]+VY[i]*VY[i];}
//Here I scaled down the velocities for aesthetic reasons
		for(int i=0;i<25;i++){VX[i]*= 0.5;VY[i]*=0.5;}
	

		menuBar.add(View);
		menuBar.add(InputMethod);
		menuBar.add(interactionsList);
	
		
//When the initial conditions are reset newWave is set to true and is only changed back once Lesson 1 has noticed it
//When the user first starts reseting conditions lesson 1 is told that it should not be recording this wave any more
//TODO check whether recordingWave actually needs to be set here...
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {
				recordingWave = false;newWave = true;}
			@Override
			public void mouseReleased(MouseEvent e) {
				newWave = true;}
		});

		add(menuBar);

		add(menuBar);
		menuBar.setLocation(5,5);
		menuBar.setVisible(true);

		zoomFactor = new JLabel(""+0);
		up = new BasicArrowButton(BasicArrowButton.NORTH);
		down = new BasicArrowButton(BasicArrowButton.SOUTH);
//Up/down arrows scale up/down the scale by a factor of 2
		up.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
				C.basicZoomFactor*=2;
			}
		});
		down.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
				C.basicZoomFactor/=2;
			}
		});
		add(up);
		add(down);
		add(zoomFactor);
		zoomFactor.setLocation(getWidth()-32, 2);
		up.setLocation(getWidth()-42,2);
		up.setSize(10,10);
		down.setLocation(getWidth()-12,2);
		down.setSize(10,10);
		zoomFactor.setSize(20, 10);


	}
	
//Here the particular cosmetics class for this sort of displayer is defined
//All that it says is how zoomed in to display the graph and what colour to draw it in
	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public Color waveNumber;
		
		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

//This adds an initial conditioner to the menu at the top
	@Override
	public void addInitialConditioner(InitialConditioner IC){
		initialConditioners.add(IC);
		Methods.add(new JRadioButtonMenuItem(IC.toString()));
		group.add(Methods.get(Methods.size()-1));
		InputMethod.add(Methods.get(Methods.size()-1));
	}
	
//This tells the wave to take the selected conditioner as its current one
//Returns true if it is a conditioner that needs to be called and false if it does not
	@Override
	public boolean setTheIC(){
		for(int j=0;j<initialConditioners.size();j++){
			if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
		}
		return true;
	}

	@Override
	protected void paintComponent(Graphics f) {
		super.paintComponent(f);
		Graphics2D g = (Graphics2D)f;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		int dataLength = ((RealDataWave)waveThing).data.x.length;
		double xScale;
		if((dataLength%2)==0){xScale = (getWidth()-40)/(dataLength-1.0);}
		else{xScale = (getWidth()-40)/(dataLength-1.0);}

		Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,dataLength);
		double yBase = getHeight()/2.+5;
		curve.moveTo(20, yBase);

		double width = (getWidth()-40.0)/(dataLength-1.)/2.;
		int height = (int) (width/2.);
		double lastX = 20+((RealDataWave)waveThing).data.x[0]/((RealDataWave)waveThing).getSpaceProps().L*(getWidth()-40);
		g.setColor(C.getWaveNumber());
		g.fillOval((int)(lastX-width/2.),(int)(yBase-width/2.), (int)width, (int)width);
		for(int i=1; i<dataLength; i++){
			double X = 20+((RealDataWave)waveThing).data.x[i]/((RealDataWave)waveThing).getSpaceProps().L*(getWidth()-40)+i*xScale;
			double delta = (X-lastX - width)/12.0;
			g.fillOval((int)(X-width/2.),(int)(yBase-width/2.), (int)width, (int)width);
			if(delta <=0){curve.lineTo(X, yBase);continue;}
			curve.lineTo(lastX+delta+width/2.,yBase);
			for(int j=1;j<6;j++){
				double X1 = width/2.+(4*j-1.0)*delta/2.0;
				double Y1 = height;
				double X2 = width/2.+2.*j*delta;
				double Y2 = 0;
				curve.quadTo(lastX+X1, yBase+Y1,lastX+X2, yBase+Y2);
				X1 = width/2.+(4*j+1.0)*delta/2.0;
				Y1 = -height;
				X2 = width/2.+(2*j+1.0)*delta;
				Y2 = 0;
				curve.quadTo(lastX+X1, yBase+Y1,lastX+X2, yBase+Y2);
			}
			curve.lineTo(lastX+width/2.+11*delta, yBase);
			curve.lineTo(X, yBase);
			lastX = X;
		}


		g.setStroke(new BasicStroke(1f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(C.getWaveNumber());
		g.draw(curve);

		g.setColor(Color.black);
		Rectangle Calorimeter = new Rectangle(getWidth()-100-1,24-1,80+2,getHeight()-35+2);
		for(int i=0;i<25;i++){
			g.fillOval(getWidth()-100+(int)(PX[i]*8.)-2, 24 + (int)(PY[i]*(getHeight()-35)/10)-2, 4, 4);
		}
		
		EnergyLevel = 0;
		for(int i=((RealDataWave)waveThing).getSpaceProps().n-1;20+i*xScale>getWidth()-100;i--){
			EnergyLevel+=((Discrete1DEnergyDensity)waveThing).energyAtIndex(i);
		}
		if(!(EnergyLevel>0)){EnergyLevel = 0;}

		g.setStroke(new BasicStroke(3f));
		g.setColor(Color.black);
		g.draw(Calorimeter);
		g.drawLine(20,25, 20,getHeight()-15);
		g.drawLine(20,getHeight()/2+5, getWidth()-20, getHeight()/2+5);
		g.setColor(Color.red);
		CenterLine = ((RealDataWave)waveThing).data.position/ ((RealDataWave)waveThing).getSpaceProps().delta -0.5;
		if(CenterLine>0){
			g.drawLine(20+(int)(CenterLine*xScale),25, 20+(int)(CenterLine*xScale),getHeight()-15);
		}

//These need to be set on each repaint to counter the null layout manager
		up.setSize(15,15);
		down.setSize(15,15);
		zoomFactor.setSize(20, 15);
		down.setLocation(getWidth()-17,2);
		zoomFactor.setLocation(getWidth()-37, 2);
		up.setLocation(getWidth()-52,2);
		menuBar.setLocation(2,2);
		menuBar.setSize(menuBar.getPreferredSize());
		menuBar.doLayout();
		g.setStroke(new BasicStroke(1f));
		g.setColor(Color.black);
	}

//Here are the required setters and getters for a CalorimeterDisplayer
	@Override
	public boolean isNewWave() {return newWave;}
	@Override
	public boolean isRecordingWave() {return recordingWave;}
	@Override
	public double getEnergyLevel() {return EnergyLevel;}
	@Override
	public void setNewWave(boolean b) {newWave = b;}
	@Override
	public void setRecordingWave(boolean b) {recordingWave = b;}

//This method scales the velocities to reflect the current energy level and the scale that the user has set on the energy bar
	@Override
	public void progressParticles(double dt, double MaxE) {
		double tmp = Math.sqrt(EnergyLevel/MaxE*totalVSq);
		for(int i=0;i<25;i++){
			PX[i]+=(dt*tmp*VX[i]);PY[i]+=(dt*tmp*VY[i]);
			if(PX[i]>10){VX[i] = -Math.abs(VX[i]);PX[i] += 2*(10-PX[i]);}
			else if(PX[i]<0){VX[i] = Math.abs(VX[i]);PX[i] *= -1;}
			if(PY[i]>10){VY[i] = -Math.abs(VY[i]);PY[i] += 2*(10-PY[i]);}
			else if(PY[i]<0){VY[i] = Math.abs(VY[i]);PY[i] *= -1;}
		}
	}
}
