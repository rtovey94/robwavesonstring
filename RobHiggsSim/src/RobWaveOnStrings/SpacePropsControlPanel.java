package RobWaveOnStrings;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
/*
 * This is the control panel which describes all customisable values which have meaning to every wave
 * They may not effect each wave at once though
 * Each input slider section has a text label, number label and a slider in the format:
 * 		|  Text  || Number |
 * 		|      Slider      |
 * The main difference between this class and the control panel is that it has a different segment for dt which isn't really a property of the space
 */
@SuppressWarnings("serial")
public class SpacePropsControlPanel extends JPanel{
	public JPanel SpaceProps, DT;
	public ArrayList<JScrollBar> parameterScrolls = new ArrayList<JScrollBar>();
	public ArrayList<JLabel> parameterNames = new ArrayList<JLabel>();
	public ArrayList<JLabel> parameterValues = new ArrayList<JLabel>();
	public JCheckBox DTPlusMinus;

	public SpacePropsControlPanel(){
//This is the container for length and n
		SpaceProps = new JPanel();
//This is the container for the dt settings
		DT = new JPanel();
		SpaceProps.setBackground(Color.lightGray);
		DT.setBackground(Color.lightGray);
		SpaceProps.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
		DT.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
		SpaceProps.setLayout(new GridBagLayout());
		DT.setLayout(new GridBagLayout());
		
		addPropertyChangeListener("refresh", new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent e){firePropertyChange("refresh and pause", e.getOldValue(), e.getNewValue());}
		});
		
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = 1;
		SpaceProps.add(new JLabel("     Space Properties"),c);
		parameterNames.add(new JLabel("   L"));
		parameterScrolls.add(new JScrollBar(JScrollBar.HORIZONTAL,36, 4, 0, 104));
		parameterValues.add(new JLabel(String.format("%3.2E", Math.pow(1.1,parameterScrolls.get(0).getValue()))));
		parameterNames.add(new JLabel("   DT"));
//TODO remove this carefully!
		parameterValues.add(new JLabel(""+0.001));
		parameterScrolls.add(new JScrollBar(JScrollBar.HORIZONTAL, 1, 4, 0, 104));
		parameterNames.add(new JLabel("   n"));
		parameterValues.add(new JLabel(""+500));
		parameterScrolls.add(new JScrollBar(JScrollBar.HORIZONTAL, 500, 4, 3, 1004));
		parameterScrolls.get(0).addAdjustmentListener(new AdjustmentListener(){
			public void adjustmentValueChanged(AdjustmentEvent e){
				parameterValues.get(0).setText(String.format("%3.2E",Math.pow(1.1,e.getValue())));
				firePropertyChange("refresh",false,true);
			}
		});
		parameterScrolls.get(1).addAdjustmentListener(new AdjustmentListener(){
			public void adjustmentValueChanged(AdjustmentEvent e){
				double Val = (double)e.getValue();
				DTPlusMinus.setText(String.format("%4.3f",Val*0.001));
			}
		});
		parameterScrolls.get(2).addAdjustmentListener(new AdjustmentListener(){
			public void adjustmentValueChanged(AdjustmentEvent e){
				parameterValues.get(2).setText(""+e.getValue());
				firePropertyChange("refresh",true,false);
			}
		});
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		SpaceProps.add(parameterNames.get(0),c);
		c.gridx = 1;
		c.gridy = 1;
		SpaceProps.add(parameterValues.get(0),c);
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		SpaceProps.add(parameterScrolls.get(0),c);

		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		SpaceProps.add(parameterNames.get(2),c);
		c.gridx = 1;
		c.gridy = 3;
		SpaceProps.add(parameterValues.get(2),c);
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 2;
		SpaceProps.add(parameterScrolls.get(2),c);

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		DT.add(parameterNames.get(1),c);
		c.gridx = 1;
		c.gridy = 0;

//Unselected icon is a plus
		DTPlusMinus = new JCheckBox("0.001", new Icon(){
			private int width = 10;
			private int height = 10;
			@Override
			public void paintIcon(Component c, Graphics f, int x, int y){
				Graphics2D g = (Graphics2D)f.create();
				g.setColor(Color.lightGray);
				g.fillRect(x,y,width,height);
				g.setStroke(new BasicStroke(2f));
				g.setColor(Color.black);
				g.drawLine(x,y+height/2,x+width,y+height/2);
				g.drawLine(x+width/2, y, x+width/2, y+height);

				g.dispose();
			}
			public int getIconWidth(){return width;}
			public int getIconHeight(){return height;}
		});
		DTPlusMinus.setBackground(Color.lightGray);
//Selected icon is a minus
		DTPlusMinus.setSelectedIcon(new Icon(){
			private int width = 10;
			private int height = 10;
			@Override
			public void paintIcon(Component c, Graphics f, int x, int y){
				Graphics2D g = (Graphics2D)f.create();
				g.setColor(Color.lightGray);
				g.fillRect(x,y,width,height);
				g.setStroke(new BasicStroke(2f));
				g.setColor(Color.black);
				g.drawLine(x,y+height/2,x+width,y+height/2);

				g.dispose();
			}
			public int getIconWidth(){return width;}
			public int getIconHeight(){return height;}
		});
		DT.add(DTPlusMinus);
		c.gridx = 2;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;
		DT.add(parameterScrolls.get(1),c);
		
		DT.setPreferredSize(new Dimension(getWidth(), 60));
		
		setLayout(new BorderLayout());
		add(SpaceProps, BorderLayout.CENTER);
		add(DT, BorderLayout.PAGE_START);
		setMinimumSize(new Dimension(200,300));
	}
}
