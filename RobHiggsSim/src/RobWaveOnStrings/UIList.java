package RobWaveOnStrings;

import java.awt.Color;
/*
 * This objects extending this class will be used by the wave type class to find out available viewers, 
 *  input methods, control panels and other methods that users will see
 */
public abstract class UIList {
//This list provides a list of the names of wave viewers
	public abstract String[] getDisplayTypes();
	
//if(fullList) return AllInteractions, else return only binary pair wave interactions
//Because of this it's much simpler to keep all binary operations at the start
	public abstract String[] getInteractions(boolean fullList);
	
//This method always also has to call setInputMethods itself because it is not called independently in either lesson
	public abstract WaveDisplayer newDisplay(WaveThing wt, Color C, int i);
//This method returns a selection of viewers that should be displayed in lesson 1
//If there is a calorimeter displayer within that list it should be at the top or it will not be detected
	public abstract WaveDisplayer[] newLesson1(WaveThing wt, Color C);
//These methods both dictate what the user sees they are changing and the conversion from user input to data
	public abstract void setInputMethods(WaveDisplayer wd, int i);
	public abstract void setInputMethods(ControlPanel CP, WaveThing wt);
//This should return users to sensible ball-park values when they switch on to this wave type 
	public abstract void checkSpacePropsControler(SpacePropsControlPanel SPCP);
//The user is the one who will initiate an interaction so here is where the method should be
//What it should do yet though is undecided
	public abstract IntegratableThing addInteraction(WaveThing[] wave,int i);

}
