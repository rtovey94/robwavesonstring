package RobWaveOnStrings;
/*
 * This is a standard initial conditioner which also corresponds a Higg's field to align with another wave
 * TODO alter control panel so that it can take this into consideration
 */
public class RSFInitialConditioner_HiggsType implements InitialConditioner {

	private RSFWaveThing rsfWaveThing_higgs;
	private RSFWaveThing rsfWaveThing_other;
	RSFAABBInteraction rsfAABBInteraction;
	
	public RSFInitialConditioner_HiggsType(
			RSFWaveThing rsfWaveThing_higgs,
			RSFWaveThing rsfWaveThing_other,
			RSFAABBInteraction rsfAABBInteraction
		) {
		this.rsfWaveThing_higgs = rsfWaveThing_higgs;
		this.rsfWaveThing_other = rsfWaveThing_other;
		this.rsfAABBInteraction = rsfAABBInteraction;
	}

	@Override
	public void setInitialCondition() {
		for (int i = 0; i < rsfWaveThing_higgs.getSpaceProps().n; ++i) {
			final double psi = rsfWaveThing_other.data.x[i];
			final double psiDot = rsfWaveThing_other.data.v[i];

			final double inside = -(rsfAABBInteraction.lambda * psi * psi + rsfWaveThing_higgs.propRSF.mSq)
					/ rsfWaveThing_higgs.propRSF.quarticTerm;
			final double insideDot = -(rsfAABBInteraction.lambda * 2 * psi * psiDot)
					/ rsfWaveThing_higgs.propRSF.quarticTerm;

			final double safeInside = inside >= 0 ? inside : 0;

			rsfWaveThing_higgs.data.x[i] = Math.sqrt(safeInside);
			rsfWaveThing_higgs.data.v[i] = 0.5 * 1 / Math.sqrt(safeInside) * insideDot;
		}
	}
	
	static public void putPacket(
			RSFWaveThing wave,
			double centralP,
			final double pSpread,
			final double centralX,
			final double norm) {

		final int bottomIp = (int) (centralP * wave.getSpaceProps().oneOverDp - 0.5 * wave.getSpaceProps().n);
		final int topIp = bottomIp + wave.getSpaceProps().n;

		final int xnCen = (int) (centralX / wave.getSpaceProps().delta);

		for (int ip = bottomIp; ip < topIp; ++ip) {

			final double pThatMultX = wave.getSpaceProps().dp * ip;

			final double pDistSigs = (pThatMultX - centralP) / pSpread;
			final double pDistSigsSq = pDistSigs * pDistSigs;

			final double weight = Math.exp(-pDistSigsSq * 0.5)
					/ (Math.sqrt(2. * Math.PI) * pSpread) * wave.getSpaceProps().dp;

			RSFInitialConditionerUtils.addModeWithMomIndexKn(wave, ip, xnCen, weight * norm);
		}
	}

	@Override 
	public RSFWaveThing getWaveThing(){
		return rsfWaveThing_other;
	}

	@Override
	public void setWaveThing(WaveThing wt){
		try{this.rsfWaveThing_other = (RSFWaveThing)wt;}
		catch(ClassCastException e){System.out.println("RSFInitialConditioner could not set wave");}
	}
	
	@Override
	public String toString(){
		return "Higgs Type IC";
	}

}
