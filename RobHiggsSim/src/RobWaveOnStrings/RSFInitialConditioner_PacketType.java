package RobWaveOnStrings;

/*
 * A typical initial conditioner for a RSF wave calls putPacket
 * This method produces a wave with given momentum
 */
public class RSFInitialConditioner_PacketType implements InitialConditioner {

	protected RSFWaveThing rsfWaveThing;
	private double normScale;
	
	public RSFInitialConditioner_PacketType(RSFWaveThing rsfWaveThing,
			double pCentral,
			double pSpread,
			double centralX,
			double normScale
		) {
		this.rsfWaveThing = rsfWaveThing;
		this.rsfWaveThing.propRSF.pCentral = pCentral;
		this.rsfWaveThing.propRSF.pSpread = pSpread;
		this.rsfWaveThing.propRSF.centralX = centralX;
		this.normScale = normScale;
	}

	@Override
	public void setInitialCondition() {
		rsfWaveThing.data.setToZero();
		
		putPacket(rsfWaveThing,
				rsfWaveThing.propRSF.pCentral, // 4.0, // pCentral
				rsfWaveThing.propRSF.pSpread, //1.0, // pSpread,
				rsfWaveThing.propRSF.centralX, // centralX,
				normScale // 1 * normScale // norm
				);

	}
	
	
	static public void putPacket(
			RSFWaveThing wave,
			double centralP,
			final double pSpread,
			final double centralX,
//Norm scale should not be used in general, it is simply a scaling factor which is actually part of the cosmetics
			final double norm) {
//The velocity is calculated here
		if(centralP==0){wave.propRSF.velocity = 0;}
		else{wave.propRSF.velocity = centralP/Math.sqrt(wave.propRSF.mSq+centralP*centralP);}
		wave.data.position = centralX;


		final int bottomIp = (int) (centralP * wave.getSpaceProps().oneOverDp - 0.5 * wave.getSpaceProps().n);
		final int topIp = bottomIp + wave.getSpaceProps().n;

		final int xnCen = (int) (centralX / wave.getSpaceProps().delta);

		for (int ip = bottomIp; ip < topIp; ++ip) {

			final double pThatMultX = wave.getSpaceProps().dp * ip;

			final double pDistSigs = (pThatMultX - centralP) / pSpread;
			final double pDistSigsSq = pDistSigs * pDistSigs;

			final double weight = Math.exp(-pDistSigsSq * 0.5)
					/ (Math.sqrt(2. * Math.PI) * pSpread) * wave.getSpaceProps().dp;

			RSFInitialConditionerUtils.addModeWithMomIndexKn(wave, ip, xnCen, weight * norm);
		}
	}
	
	@Override 
	public RSFWaveThing getWaveThing(){
		return rsfWaveThing;
	}
	
	@Override
	public void setWaveThing(WaveThing wt){
		try{this.rsfWaveThing = (RSFWaveThing)wt;}
		catch(ClassCastException e){System.out.println("RSFInitialConditioner could not set wave");}
	}
	
//This toString method is only implemented for hard coded initial conditions which won't appear in the final lesson version
	@Override
	public String toString(){
		return "Packet Type IC ( "+(float)rsfWaveThing.propRSF.pCentral+", "+(float)rsfWaveThing.propRSF.pSpread+", "+(float)rsfWaveThing.propRSF.centralX+" )";
	}
	

}
