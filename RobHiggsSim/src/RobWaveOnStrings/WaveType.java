package RobWaveOnStrings;
/*
 * This is the class that every wave type must extend
 */
public abstract class WaveType {
//Every wave type must have a UIList object which dictates viewers, control panels and input methods
	public UIList inputDisplays;
//Must have a method to return a fresh new wave
	public abstract WaveThing createNewWave(SpaceProperties propSpace, LocalProperties localProp);
//Must have an appropriately concise and unique string conversion for users to see 
	public abstract String toString();
//Must be able to return a default copy of local properties
	public abstract LocalProperties LocalProps();
	
}
