package RobWaveOnStrings;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.EventListener;


@SuppressWarnings({ "serial", "unused" })
public class Lesson2 extends JPanel{
	public ArrayList<WaveType> WaveTypeList;
	
	Integrator integrator =  new LeapfrogIntegrator();
	
	private JPanel controlViewer;
	private JPanel waveViewer;
	private WaveThing CurrentWave;
	private ArrayList<WaveDisplayer> WaveDisplay = new ArrayList<WaveDisplayer>();
	private double DT = 0.01;
	private ControlPanel waveControlVariables = new ControlPanel();
	private SpacePropsControlPanel propSpaceVariables = new SpacePropsControlPanel();
	private JMenuBar NewButtons = new JMenuBar(), MovePanels = new JMenuBar();
	private JMenu AddWave = new JMenu("Add new wave"), AddView = new JMenu("Add viewer to current wave"), AddInteraction = new JMenu("Add new interaction");
	private JButton RemoveWave = new JButton("Remove current viewer");
	private int hasFocus;
	public Timer myTimer =new Timer(30, new ActionListener(){
		public void actionPerformed(ActionEvent e){repaint();
			for(int i=0;i<WaveDisplay.size();i++){
				if(WaveDisplay.get(i).isAdjusting){return;}
			}
			double delta = Math.abs(DT);
			if(DT!=0){while(delta<=0.1){integrator.advanceAll(DT);delta+=Math.abs(DT);}integrator.advanceAll((0.1)%DT);}
		}});

	public Lesson2(ArrayList<WaveType> WTL){/*throws Exception{
		try{int n = WTL.size();}catch(Exception e){
			throw new Exception("No Wave Types Have Been Added");
			}*/
		setLayout(null);
		
		this.WaveTypeList = WTL;
		 
		constructWaveViewer();
		constructControlViewer();
		 
		add(waveViewer);
		add(controlViewer);
		propSpaceVariables.SpaceProps.addPropertyChangeListener("reset and pause", new PropertyChangeListener(){
			 public void propertyChange(PropertyChangeEvent e){WaveDisplay.get(hasFocus).waveThing.setSpaceProps(new SpaceProperties(propSpaceVariables.parameterScrolls.get(2).getValue(), propSpaceVariables.parameterScrolls.get(0).getValue()));
			 WaveDisplay.get(hasFocus).waveThing.getTheIC().setInitialCondition();//TODO pauseWave.setSelected(true);
			 }
		});
		propSpaceVariables.DTPlusMinus.addPropertyChangeListener(new PropertyChangeListener(){
			 public void propertyChange(PropertyChangeEvent e){
				 DT = Double.parseDouble(propSpaceVariables.DTPlusMinus.getText());
				 if(propSpaceVariables.DTPlusMinus.isSelected()){DT*=-1;}
			 }
		});
		propSpaceVariables.DTPlusMinus.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				 DT = Double.parseDouble(propSpaceVariables.DTPlusMinus.getText());
				 if(propSpaceVariables.DTPlusMinus.isSelected()){DT*=-1;}
			}
		});

		 
		 KeyboardFocusManager.getCurrentKeyboardFocusManager().addPropertyChangeListener(
				 new PropertyChangeListener(){
					@Override
					public void propertyChange(PropertyChangeEvent evt) {
						int i;
						for(i=0;i<WaveDisplay.size();++i){
							if(WaveDisplay.get(i).hasFocus()){
								CurrentWave = WaveDisplay.get(i).waveThing;
								getCurrentWaveType().inputDisplays.setInputMethods(waveControlVariables, CurrentWave);
								hasFocus = i;
								break;}
						}
						if(i==WaveDisplay.size()){return;}
						for(int j=0;j<WaveDisplay.size();j++){
							if(j==i){
								WaveDisplay.get(j).setFocus(true);
							}
							else{
								WaveDisplay.get(j).setFocus(false);
							}
						}
						reconstructAddButtons();
					}
				}
		 );
		 
		 setMinimumSize(new Dimension(700,500));
		 setBackground(Color.white);
		 setOpaque(true);
		 setVisible(true);
		 
		 WaveDisplay.get(0).setFocus(true);
		 resetPanels();
		 myTimer.start();
	}

	 private void constructWaveViewer(){
		waveViewer = new JPanel();
		waveViewer.setLayout(new GridLayout(0,1));

		createNewWave(WaveTypeList.get(0).toString());
//TODO sort out integrator things...
		integrator.add(CurrentWave);		
		
		addWaveViewer(1);
	 }
	 
	 private void constructControlViewer(){
		controlViewer = new JPanel();
		controlViewer.setLayout(null);
			
		controlViewer.add(waveControlVariables);
		controlViewer.add(propSpaceVariables);
		JButton MoveUp = new JButton("Up");
		MoveUp.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(hasFocus!=0){
					WaveDisplayer tmp = WaveDisplay.remove(hasFocus-1);
					WaveDisplay.add(hasFocus,tmp);
					waveViewer.removeAll();
					for(int i=0;i<WaveDisplay.size();i++){waveViewer.add(WaveDisplay.get(i));}
					hasFocus-=1;
					waveViewer.validate();
				}
				
			}
		});
		JButton MoveDown = new JButton("Down");
		MoveDown.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(hasFocus!=WaveDisplay.size()-1){
					WaveDisplayer tmp = WaveDisplay.remove(hasFocus);
					WaveDisplay.add(hasFocus+1,tmp);
					waveViewer.removeAll();
					for(int i=0;i<WaveDisplay.size();i++){waveViewer.add(WaveDisplay.get(i));}
					hasFocus+=1;
					waveViewer.validate();
				}
				
			}
		});
		MovePanels.setLayout(new BorderLayout());
		MovePanels.add(MoveUp, BorderLayout.LINE_START);
		MovePanels.add(new JLabel("     Shift current panel"),BorderLayout.CENTER);
		MovePanels.add(MoveDown, BorderLayout.LINE_END);
		controlViewer.add(MovePanels);
		
		RemoveWave.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{removeWaveViewer(WaveDisplay.get(hasFocus));}catch(ArrayIndexOutOfBoundsException exc){}
			}
		});
		NewButtons.setLayout(new GridLayout(0,1));
		NewButtons.add(AddWave);
		NewButtons.add(AddView);
		NewButtons.add(AddInteraction);
		NewButtons.add(RemoveWave);
		controlViewer.add(NewButtons);
		
		reconstructAddButtons();
	 }
	 
	 private void createNewWave(String Type){
		 SpaceProperties propSpace = new SpaceProperties(Integer.parseInt(propSpaceVariables.parameterValues.get(2).getText()),Double.parseDouble(propSpaceVariables.parameterValues.get(0).getText()));
		 CurrentWave = getWaveType(Type).createNewWave(propSpace, getWaveType(Type).LocalProps());
			 
//TODO sort out for integrators
		 integrator.add(CurrentWave);		
	 }
	 
	private void addWaveViewer(int i){
		WaveDisplay.add(getCurrentWaveType().inputDisplays.newDisplay(CurrentWave, findNewColour(), i));
		final int n = WaveDisplay.size()-1;
		 waveViewer.add(WaveDisplay.get(n));
		 setViewerListener(n);
		 for(int j=0;j<n;j++){WaveDisplay.get(j).setFocus(false);}
		 hasFocus = n;
		 WaveDisplay.get(n).setFocus(true);
		 waveViewer.validate();
	 }
	 
	 private void removeWaveViewer(WaveDisplayer WD){
		waveViewer.remove(WD);
		WaveDisplay.remove(WD);
		waveViewer.doLayout();
		try{WaveDisplay.get(0).hasFocus();}catch(IndexOutOfBoundsException e){CurrentWave = null;}
//TODO fix to check nullity of CurrentWave
		hasFocus = 0;
		reconstructAddButtons();
	 }
	 
	 private void reconstructAddButtons(){
		 String[] tmp;
		 AddWave.removeAll();
		 for(int i=0;i<WaveTypeList.size();i++){
			 final int I = i;
			 JMenu tmpMenu = new JMenu(WaveTypeList.get(i).toString());
			 tmp = WaveTypeList.get(i).inputDisplays.getDisplayTypes();
			 for(int j=0;j<tmp.length;j++){
				 final int k = j;
				 tmpMenu.add(new JMenuItem(tmp[k]));
				((JMenuItem)tmpMenu.getMenuComponent(k)).addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						createNewWave(WaveTypeList.get(I).toString());
						addWaveViewer(k+1);
					}
				});
			 }	
			 
			 AddWave.add(tmpMenu);
		 
			((JMenu)AddWave.getMenuComponent(i)).addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e){
					String tmp =((JMenu)(e.getSource())).getText(); 
					if(tmp!=AddWave.getText()){createNewWave(tmp);addWaveViewer(1);}
				}
			});
			
		 }
		 
		 AddView.removeAll();
		 try{tmp = getCurrentWaveType().inputDisplays.getDisplayTypes();
		 for(int i=0;i<tmp.length;i++){
			 AddView.add(new JMenuItem(tmp[i]));
			 final int j = i;
			((JMenuItem)AddView.getMenuComponent(j)).addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e){
					addWaveViewer(j+1);
				}
			});
			
		 }		
		 }catch(NullPointerException e){}
		 
		 AddInteraction.removeAll();
//TODO using current as base add each other wave number(?) to menu as menus?
//TODO then add a list of items
		 
	 }
	 
	 private void setViewerListener(int i){
		final int n = i;
		WaveDisplay.get(n).addPropertyChangeListener("refresh", new PropertyChangeListener(){
				@Override
				public void propertyChange(PropertyChangeEvent e){waveViewer.repaint();}
			});

		waveViewer.getComponent(n).addMouseListener(new MouseListener(){
			@Override
			public void mouseClicked(MouseEvent e) {
				if((e.getModifiers()&InputEvent.BUTTON3_MASK)==InputEvent.BUTTON3_MASK){
				if(WaveDisplay.get(n).waveThing!=WaveDisplay.get(hasFocus).waveThing){
				}
				}
			}
			@Override
			public void mousePressed(MouseEvent e) {
				if((e.getModifiers()&InputEvent.BUTTON3_MASK)==InputEvent.BUTTON3_MASK){WaveDisplay.get(n).InteractionMenu.show(e.getComponent(),e.getX(),e.getY());
				if(n!=hasFocus){
					makeQuickInteractions(n, hasFocus);
					WaveDisplay.get(n).InteractionMenu.repaint();
				}
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {}});
		 
	 }
	 
	 private void makeQuickInteractions(int i, int j){
		 if((WaveDisplay.get(i).waveThing==WaveDisplay.get(j).waveThing)||(WaveDisplay.get(i).waveThing.typeToString()!=WaveDisplay.get(j).waveThing.typeToString())){
			 WaveDisplay.get(i).InteractionMenu.removeAll();
			 WaveDisplay.get(i).InteractionMenu.add(new JLabel("No interactions"));
			 WaveDisplay.get(i).InteractionMenu.add(new JLabel(" available"));
			 WaveDisplay.get(i).InteractionMenu.repaint();
			 return;}
		 	String[] ints = getCurrentWaveType().inputDisplays.getInteractions(false);
			WaveDisplay.get(i).InteractionMenu.removeAll();

			try{WaveDisplay.get(i).InteractionMenu.add(WaveDisplay.get(i).Interaction);
				for(int k=0;k<ints.length;k++){
					JMenuItem tmp = new JMenuItem(ints[k]);
					tmp.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent e) {
						}
					});
					WaveDisplay.get(i).Interaction.add(tmp);
				}
			}catch(Exception e){
				WaveDisplay.get(i).InteractionMenu.add(new JLabel("No interactions"));
				WaveDisplay.get(i).InteractionMenu.add(new JLabel(" available"));
			}
			 WaveDisplay.get(i).InteractionMenu.repaint();
	 }
	 
	 public void resetPanels(){
		 waveViewer.setSize(getWidth()-250,getHeight());
		 controlViewer.setSize(250,getHeight());
		 waveControlVariables.setSize(250, (int)(3*(getHeight()-100.)/5));
		 propSpaceVariables.setSize(250, (int)(2*(getHeight()-100.)/5));
		 MovePanels.setSize(250,25);
		 NewButtons.setSize(250, 75);

		 
		 
		 waveViewer.setLocation(0,0);
		 controlViewer.setLocation(getWidth()-250,0);
		 waveControlVariables.setLocation(0, 0);
		 propSpaceVariables.setLocation(0, (int) (3*(getHeight()-100.)/5));
		 MovePanels.setLocation(0,getHeight()-100);
		 NewButtons.setLocation(0,getHeight()-75);
		 
	 }
	 
	 private WaveType getCurrentWaveType(){
			for(int j=0; j<WaveTypeList.size();j++){
				if(WaveTypeList.get(j).toString()==CurrentWave.typeToString()){
					return WaveTypeList.get(j);}
			}
			System.out.println("getCurrentWaveType(string) has returned null");
			return null;	
	 }

	 private WaveType getWaveType(String type) {
			for(int j=0; j<WaveTypeList.size();j++){
				if(WaveTypeList.get(j).toString()==type){
					return WaveTypeList.get(j);}
			}
		System.out.println("getWaveType(string) has returned null");
		 return null;
	}
	
private static final Color[] ColorList = {Color.black, Color.blue, new Color(14,161,19), Color.cyan, new Color(255,102,0), new Color(149, 16, 189), new Color(153,204,0), new Color(255,51,153)};
	 private Color findNewColour(){
		 for(int j=0;j<WaveDisplay.size();j++){
			if(WaveDisplay.get(j).waveThing==CurrentWave){return WaveDisplay.get(j).C.getWaveNumber();}
		 }
		 for(int i=0;i<ColorList.length;i++){
			 int j;
			 for(j=0;j<WaveDisplay.size();j++){
				if(WaveDisplay.get(j).C.getWaveNumber()==ColorList[i]){break;}
			 }
			 if(j==WaveDisplay.size()){return ColorList[i];}
		 }
		return Color.gray;
	 }

}
