package RobWaveOnStrings;
/*
 * A Leapfroggable interaction will always have a form that just effects V based on current X 
 */
public interface LeapfroggableInteraction extends IntegratableInteraction{
	public void advanceVBasedOnX(double dt);
}
