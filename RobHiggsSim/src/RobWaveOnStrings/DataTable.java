package RobWaveOnStrings;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.Icon;
import javax.swing.JMenu;
/*
 * This is the object which stores all of the data values that go into the data table
 * It initiates the table filled with null values and values are set as they go
 * Various methods for the JTable model are held here as well
 */
public class DataTable {

	public Object[][] data;
	public Icon[] Key = new Icon[20];
	public ResetButton[] resetButtons;

//If you want to add another column to the table then after you have added it to the column titles in lesson 1
// add another column to data in its initiation and then try to insert it somewhere in the middle
	DataTable(int n){
		this.data = new Object[n][11];
		this.resetButtons = new ResetButton[n];
		initiateKeys();
		for(int i=0;i<data.length;i++){
			if(i<Key.length){data[i][0] = (Icon)Key[i];}
			else{
			data[i][0] = (Icon)customKey(new BasicStroke(2f), Color.white);}
			data[i][1] = new Integer(i+1);
			data[i][2] = new String("");
			data[i][3] = new String("");
			data[i][4] = new String("");
			data[i][5] = new String("");
			data[i][6] = new String("");
			data[i][7] = new String("");
			data[i][8] = new String("");
			data[i][data[0].length-2] = new JMenu("");
			resetButtons[i] = new ResetButton(new Object[]{"",null,null,null});
			data[i][data[0].length-1] = resetButtons[i];
		}
	}


//-----------------------------------------------------------
//Here are the JTable model methods
	public int getRowCount(){return data.length;}

	public Object getValueAt(int r, int c){
		return data[r][c];
	}

	@SuppressWarnings("rawtypes")
	public Class getColumnClass(int c){return getValueAt(0,c).getClass();}
	
	public void setValueAt(Object val, int r, int c){
		if(c==data[0].length-1){resetButtons[r] = new ResetButton((Object[])val);}
		else{data[r][c] = val;}
	}

//-----------------------------------------------------------
//This method returns an icon with a simple horizontal line across it in designated colour and style
	public Icon customKey(final BasicStroke s,final Color col){
		 return new Icon(){
			private int width = 60;
			private int height = 20;
			@Override
			public void paintIcon(Component c, Graphics f, int x, int y){
				Graphics2D g = (Graphics2D)f.create();
				g.setColor(Color.white);
				g.fillRect(x,y,width,height);
				g.setStroke(s);
				g.setColor(col);
				g.drawLine(x,8,x+width,8);

				g.dispose();
			}
			public int getIconWidth(){return width;}
			public int getIconHeight(){return height;}
		};
	}
	
//This method just initiates all of the line types that I use in lesson 1
//There are 10 colours which are duplicated with dashed lines
	public void initiateKeys(){
		Key[0] = customKey(new BasicStroke(2f), Color.black);
		Key[1] = customKey(new BasicStroke(2f), Color.blue);
		Key[2] = customKey(new BasicStroke(2f), new Color(14,161,19));
		Key[3] = customKey(new BasicStroke(2f), new Color(149,16,189));
		Key[4] = customKey(new BasicStroke(2f), new Color(128,128,128));
		Key[5] = customKey(new BasicStroke(2f), Color.red);
		Key[6] = customKey(new BasicStroke(2f), Color.cyan);
		Key[7] = customKey(new BasicStroke(2f), new Color(153,204,0));
		Key[8] = customKey(new BasicStroke(2f), new Color(255,102,0));
		Key[9] = customKey(new BasicStroke(2f), new Color(255,51,153));
		Key[10] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), Color.black);
		Key[11] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), Color.blue);
		Key[12] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), new Color(14,161,19));
		Key[13] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), new Color(149,16,189));
		Key[14] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), new Color(128,128,128));
		Key[15] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), Color.red);
		Key[16] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), Color.cyan);
		Key[17] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), new Color(153,204,0));
		Key[18] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), new Color(255,102,0));
		Key[19] = customKey(new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f), new Color(255,51,153));

	}
}
