package RobWaveOnStrings;
/*
 * The RSF interface list describes 6 different display types
 * It also gives information to form its control panel and to check the space properties
 */
import java.awt.Color;

public class RSFInterfaceList extends UIList{
	public final static int RSF_AmplitudeWithCal =0;
	public final static int RSF_Amplitude =1;
	public final static int RSF_Velocity=2;
	public final static int RSF_Energy =3;
	public final static int RSF_3DAmplitude =4;
	public final static int RSF_3DWave =5;
	public final static String[] Display = {"Amplitude", "Velocity", "Energy", "3D Amplitude", "3D Wave"};

	public String[] getDisplayTypes() {
		return Display;
	}

	@Override
	public String[] getInteractions(boolean fullList) {
		if(fullList){return new String[]{"AB","AABB","ABC"};}
		else{return new String[]{"AB", "AABB"};}
	}

	public WaveDisplayer newDisplay(WaveThing wt, Color C, int i) {
		WaveDisplayer WD = null;
		RSFWaveThing WT;
		try{WT = (RSFWaveThing)wt;
		switch(i){
		case 0:	WD = new StringWaveAmplitudeWithCal(WT, new StringWaveAmplitudeWithCal.OurCosmetics(C));
				((StringWaveAmplitudeWithCal)WD).View.setText("Amplitude with calorimeter");
				((StringWaveAmplitudeWithCal)WD).Type.setText("Klein Gordon");
				break;
		case 1:	WD = new StringWaveAmplitude(WT, new StringWaveAmplitude.OurCosmetics(C));
				((StringWaveAmplitude)WD).View.setText(Display[i-1]);
				((StringWaveAmplitude)WD).Type.setText("Klein Gordon");
				break;
		case 2: WD = new StringWaveVelocity(WT, new StringWaveVelocity.OurCosmetics(C));
				((StringWaveVelocity)WD).View.setText(Display[i-1]);
				((StringWaveVelocity)WD).Type.setText("Klein Gordon");
				break;
		case 3: WD = new StringWaveEnergy(WT, new StringWaveEnergy.OurCosmetics(C));
				((StringWaveEnergy)WD).View.setText(Display[i-1]);
				((StringWaveEnergy)WD).Type.setText("Klein Gordon");
				break;
		case 4: /*WD = new StringWaveAmplitude3D(WT, new StringWaveAmplitude3D.OurCosmetics(C));
				((StringWaveAmplitude3D)WD).View.setText(Display[i-1]);
				((StringWaveAmplitude3D)WD).Type.setText("Klein Gordon");
				*/WD = new StringWaveAmplitude3DVersion2(WT, new StringWaveAmplitude3DVersion2.OurCosmetics(C));
				break;
		case 5: WD = new StringWave3DRepresentation(WT, new StringWave3DRepresentation.OurCosmetics(C));
				((StringWave3DRepresentation)WD).View.setText(Display[i-1]);
				((StringWave3DRepresentation)WD).Type.setText("Klein Gordon");
				((StringWave3DRepresentation)WD).C.basicZoomFactor1 = 20;
				((StringWave3DRepresentation)WD).C.basicZoomFactor2 = 20;
				break;
		}
		if(i>5||i<0){System.out.println("newDisplay() has returned null in RSF UI");}
			setInputMethods(WD, i);
			return WD;
			}catch(Exception e){
				System.out.println("newDisplay() has returned null in RSF UI");
				return null;}
	}

	@Override
	public WaveDisplayer[] newLesson1(WaveThing wt, Color C){
		WaveDisplayer[] tmp = new WaveDisplayer[]{newDisplay(wt,C,0),newDisplay(wt,C,3), newDisplay(wt,C,5)};
//		tmp[0].waveThing.getSpaceProps().restrictedView = true;
//		tmp[1].waveThing.getSpaceProps().restrictedView = true;
//		tmp[2].waveThing.getSpaceProps().restrictedView = true;
		return tmp;		
	}
	
	public void setInputMethods(WaveDisplayer wd, int i) {
		RealDataWaveDisplayer WD;
		try{WD = (RealDataWaveDisplayer)wd;}catch(Exception e){return;}
		switch(i){
		case 0:	WD.addInitialConditioner(new RSFInitialConditioner_PacketType((RSFWaveThing) WD.waveThing,4,1,5,1));
				WD.addInitialConditioner(new RSFInitialConditioner_PacketTypeWithCatapultXandStretchY(WD, (RSFWaveThing) WD.waveThing));
				((StringWaveAmplitudeWithCal)WD).Methods.get(1).setSelected(true);
				break;
		case 1:	WD.addInitialConditioner(new RSFInitialConditioner_PacketType((RSFWaveThing) WD.waveThing,4,1,5,1));
				WD.addInitialConditioner(new RSFInitialConditioner_PacketTypeWithCatapultXandStretchY(WD, (RSFWaveThing) WD.waveThing));
				((StringWaveAmplitude)WD).Methods.get(1).setSelected(true);
				break;
		case 2: WD.addInitialConditioner(new RSFInitialConditioner_PacketType((RSFWaveThing) WD.waveThing,4,1,5,1));
				WD.addInitialConditioner(new RSFInitialConditioner_PacketTypeWithCatapultXandStretchY(WD, (RSFWaveThing) WD.waveThing));
				((StringWaveVelocity)WD).Methods.get(1).setSelected(true);
				break;
		}
	}
	
	public void setInputMethods(ControlPanel CP, WaveThing wt){
		RSFWaveThing WT;
		try{WT = (RSFWaveThing)wt;
		String[] paramNames = {"Mass squared", "Momentum", "Centre", "Spread"};
		int[] lowerBound = { 0, -100, 0, 0};
		int[] init = { (int)((WT.propRSF.mSq)*10), (int)(WT.propRSF.pCentral/0.04), (int)(WT.propRSF.centralX/WT.getSpaceProps().L*100.), (int)(WT.propRSF.pSpread*10)};
		if(WT.propRSF.pCentral>4){init[1] = 4;}
		if(WT.propRSF.pCentral<-4){init[1] = -4;}
		if(WT.propRSF.pSpread<0){init[3] = 0;}
		if(WT.propRSF.pSpread>10){init[3] = 10;}
		int[] upperBound = { 50, 100, 100, 40};
		double[] scale = {0.5, 0.04, WT.getSpaceProps().L/120., 0.1};
		CP.initiateScrollBars( paramNames, lowerBound, init, upperBound, scale);
		CP.TheIC = new RSFInitialConditioner_ControlPanel(CP,WT);
		}
		catch(Exception e){
			if(CP!=null){
			System.out.println("setInputMethods() returned null in RSFInterface");}
		}
	}
	
	@Override
	public void checkSpacePropsControler(SpacePropsControlPanel SPCP){
		if(SPCP.parameterScrolls.get(0).getValue()<10||SPCP.parameterScrolls.get(0).getValue()>60){SPCP.parameterScrolls.get(0).setValue(30);}
		if(SPCP.parameterScrolls.get(2).getValue()<100||SPCP.parameterScrolls.get(2).getValue()>800){SPCP.parameterScrolls.get(2).setValue(500);}
	}

	@Override
	public IntegratableThing addInteraction(WaveThing[] wave, int i) {
//TODO or not TODO...
		return null;
	}

}
