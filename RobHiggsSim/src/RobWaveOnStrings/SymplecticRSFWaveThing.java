package RobWaveOnStrings;

public class SymplecticRSFWaveThing  extends RSFWaveThing {
	private final int ORDER = 2;

	public SymplecticRSFWaveThing(SpaceProperties propSpace, RSFProperties propRSF) {
		super(propSpace,propRSF);
	}

	@Override
	public void advance(double dt) {
		switch(ORDER){
		case 2: compose(dt);
			break;
		case 4:
			double[] w = {0.28,0.62546642846767004501,-0.81093285693534};
			compose(w[0]*dt);
			compose(w[1]*dt);
			compose(w[2]*dt);
			compose(w[1]*dt);
			compose(w[0]*dt);
			break;
		case 6:
			double[] x = {0.392161444007314143928,0.33259913678935943860,-0.70624617255763935981,0.08221359629355080023,0.79854399093483};
			for(int i=0;i<5;i++){compose(x[i]*dt);}
			for(int i=3;i>-1;i--){compose(x[i]*dt);}
			break;
		case 8:
			double[] y = {0.74167036435061295345,-0.409100825800031594,0.190754710296238137995,-0.5738624711608226666,0.29906418130365592384,0.33462491824529818378,0.31529309239676659663,-0.796887939352916};
			for(int i=0;i<7;i++){compose(y[i]*dt);}
			for(int i=6;i>-1;i--){compose(y[i]*dt);}
			break;
		
		}
	}
	
	private void compose(double dt){
		if(dt>0){
			advanceVBasedOnX(dt);
			advanceXBasedOnV(dt);
		}
		else{
			advanceXBasedOnV(dt);
			advanceVBasedOnX(dt);
		}
	}
}