package RobWaveOnStrings;

public class BOSInitialConditioner_ControlPanel extends BOSInitialConditioner{
	/*
	 * This initial conditioner is for the control panel only and should be able to set every variable that the user can set
	 * Similarly every wave that can be produced by the user should also be reproducible here
	 */
//The relevant control panel is kept track of as well as the wave so that it knows where to get its reset values from
private ControlPanel CP;

	public BOSInitialConditioner_ControlPanel(ControlPanel CP, BOSWaveThing bosWT){
		super(bosWT, 1,0);
		this.CP = CP;
	}
	
	private void resetWave(){
	//This method sets every variable that can be set with a couple of conditional tweaks
		bosWaveThing.propBOS.totalMass = 0.1*((double)CP.parameterScrolls.get(0).getValue());
		bosWaveThing.propBOS.focusBall = CP.parameterScrolls.get(1).getValue();
		bosWaveThing.propBOS.endPosition = ((double)bosWaveThing.propBOS.focusBall)/(bosWaveThing.getSpaceProps().n-1.0)+((double)CP.parameterScrolls.get(2).getValue())/50.0;
		if(bosWaveThing.propBOS.endPosition<0){bosWaveThing.propBOS.endPosition = 0.;}
		else if(bosWaveThing.propBOS.endPosition>1){bosWaveThing.propBOS.endPosition = 1;}
//TODO Potential field?
		bosWaveThing.propBOS.K = 0.1*((double)CP.parameterScrolls.get(4).getValue());
		
	}
	
	@Override
	public void setInitialCondition(){
	//Here setting the initial conditions also includes checking that variables have the correct values
		bosWaveThing.data.setToZero();
		resetWave();
		putPacket();
	}

}