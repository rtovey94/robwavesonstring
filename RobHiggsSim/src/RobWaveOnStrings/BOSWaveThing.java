package RobWaveOnStrings;

import java.util.Hashtable;

//Here I declare that a BOS wave has real data, is integrateable with a leap-frog method and has discrete energy density
public class BOSWaveThing extends RealDataWave implements LeapfroggableWaveThing,Discrete1DEnergyDensity{
//Stores the distinct properties for this wave and the last set of initial conditions it had
	public BOSProperties propBOS;
	public BOSInitialConditioner theIC;
	
	public BOSWaveThing(SpaceProperties propSpace, BOSProperties propBOS){
		super(propSpace);
		this.propBOS = propBOS;
		this.theIC = new BOSInitialConditioner(this,1,0);
	}
	
//The ability to flatten the wave is also stored in the real data class
	@Override
	public void zeroTheWave() {
		data.setToZero();	
	}
	
//Here is the reflection of the WaveType toString method
	@Override
	public String typeToString() {
		return "Balls on Springs";
	}
	
//Discrete energy calculation
	@Override
	public double energyAtIndex(int index) {
//TODO needs to be looked at... very low energy conservation
		double Potential = 0;
		if(index !=0){Potential = 0.5*propBOS.K*Math.pow(data.x[index-1]-data.x[index],2);}
		return 0.5*propBOS.totalMass/(double)getSpaceProps().n*data.v[index]*data.v[index] + Potential;
	}
	
//can be in a Leap-frog integrator:
	@Override
	public void advanceVBasedOnX(double dt) {
		double mass = propBOS.totalMass/(double)getSpaceProps().n; 
		for (int i = 1; i < data.x.length-1; ++i) {

			final double VDot = (data.x[i+1] + data.x[i-1] - data.x[i] * 2.0)
					*getSpaceProps().delta*getSpaceProps().delta*propBOS.K/mass;

			data.v[i] += VDot*dt;
		}
	
	}
	@Override
	public void advanceXBasedOnV(double dt) {
		for (int i = 0; i < data.x.length; ++i) {
			data.x[i] += dt * data.v[i];
		}
		data.totalT += dt;
		advancePosition(0);
	}
	
//Here I define position to be the mode of the energy
	@Override
	public void advancePosition(double dt){
		int j=0;
		double Etot = 0;
		for(int i=0;i<getSpaceProps().n;i++){Etot+=energyAtIndex(i);}
		if(Etot<0.00001){return;}
		double tmpE = 0;
		while(tmpE<Etot/2){tmpE+=energyAtIndex(j);j++;}
		tmpE-=energyAtIndex(j-1);
		data.position = (j-1 + (Etot/2-tmpE)/(energyAtIndex(j-1)))*getSpaceProps().delta;
	}
	
//To advance a leap-frog component this code is always how to advance the whole
	@Override
	public void advance(double dt) {
		advanceVBasedOnX(dt);
		advanceXBasedOnV(dt);
	}

//The defining/ interesting data for a BOS wave is: total mass, type, any potential field, the spring constant, total energy and mass per ball.
//Energy in the calorimeter is also displayed in the table
	@Override
	public Hashtable<String,String> getLesson1DataSet() {
		Hashtable<String,String> tmp = new Hashtable<String,String>();
		tmp.put("Mass", String.format("%3.2f", propBOS.totalMass));
		tmp.put("Type", typeToString());
		tmp.put("Potential Field", ""+propBOS.potential);
		tmp.put("Spring Constant", String.format("%3.2f", propBOS.K));
		double tmpE = 0;for(int i=0;i<getSpaceProps().n;i++){tmpE+=energyAtIndex(i);}
		tmp.put("Energy", String.format("%4.3E", tmpE));
		tmp.put("Mass per ball", String.format("%3.2f", propBOS.totalMass/(double)getSpaceProps().n));
		return tmp;
	}

//getters and setters for initial conditioners and property sets
	@Override
	public void setTheIC(InitialConditioner IC) {try{theIC = (BOSInitialConditioner)IC;}catch(Exception e){}}
	@Override
	public BOSInitialConditioner getTheIC() {return theIC;}
	@Override
	public SpaceProperties getSpaceProps() {return propSpace;}
	@Override
	public BOSProperties getLocalProps() {return propBOS;}
	@Override
	public void setSpaceProps(SpaceProperties SP) {this.propSpace = SP;this.data.resize(SP.n);if(propBOS.focusBall>=SP.n){propBOS.focusBall=1;}}
	@Override
	public void setLocalProps(LocalProperties LP) {try{this.propBOS = (BOSProperties)LP;}catch(Exception e){}}

//Gives the position of the wave when requested
	@Override
	public double getPosition() {
		return data.position;
	}
}
