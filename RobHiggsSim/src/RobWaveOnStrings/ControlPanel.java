package RobWaveOnStrings;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
/*
 * This class is the flexible model of a general control panel
 * It can be reset many times to have any number of input slider sections
 * Each input slider section has a text label, number label and a slider in the format:
 * 		|  Text  || Number |
 * 		|      Slider      |
 */
@SuppressWarnings("serial")
public class ControlPanel extends JPanel{
	public InitialConditioner TheIC;
	public ArrayList<JScrollBar> parameterScrolls = new ArrayList<JScrollBar>();
	public ArrayList<JLabel> parameterNames = new ArrayList<JLabel>();
	public ArrayList<JLabel> parameterValues = new ArrayList<JLabel>();
	
	public ControlPanel(){
		setBackground(Color.lightGray);	
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
		removeAll();add(new JLabel("Wave Properties"));
	}
	
//This method clears all previous memory and resets it with the new preferred set of sliders and values
//As sliders only support int values there is the double scale factor to convert it to the real value
	public void initiateScrollBars(String[] ParamNames, int[] lowerBound, int[] initial, int[] upperBound, final double[] scale){
		removeAll();
		parameterScrolls.clear();
		parameterNames.clear();
		parameterValues.clear();
		invalidate();
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = 1;
		add(new JLabel("Wave Properties"),c);
		for(int i=0; i<ParamNames.length; i++){
			parameterNames.add(new JLabel(ParamNames[i]));
			parameterValues.add(new JLabel(String.format("%4.3f",((double)initial[i])*scale[i])));
			parameterScrolls.add(new JScrollBar(JScrollBar.HORIZONTAL, initial[i], 4, lowerBound[i], upperBound[i]+4));
			final int j = i;
			parameterScrolls.get(j).addAdjustmentListener(new AdjustmentListener(){
				public void adjustmentValueChanged(AdjustmentEvent e){
					double Val = (double)e.getValue();
					parameterValues.get(j).setText(String.format("%4.3f",Val*scale[j]));
					TheIC.getWaveThing().setTheIC(TheIC);
					TheIC.setInitialCondition();
					firePropertyChange("refresh and pause",true,false);
					validate();
				}
			});

			c.gridx = 0;
			c.gridy = 2*i+1;
			c.gridwidth = 1;
			add(parameterNames.get(i),c);
			c.gridx = 1;
			c.gridy = 2*i+1;
			add(parameterValues.get(i),c);
			c.gridx = 0;
			c.gridy = 2*i+2;
			c.gridwidth = 2;
			add(parameterScrolls.get(i),c);
		}
	}
	
}
