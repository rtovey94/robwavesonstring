package RobWaveOnStrings;

/*
 * Core class which every BOS wave uses to set itself. It should also be sufficient to hard wire a set of initial conditions.
 * Any method of initiation should be seen here and user initialisers should know 
 * how to convert user values into data usable by this class.
 */

public class BOSInitialConditioner implements InitialConditioner{
	//The current values needed for setting conditions are all stored in the local variables so 
	//that they are stored in one fixed place rather than independent conditioners
	protected BOSWaveThing bosWaveThing;
	
	public BOSInitialConditioner(BOSWaveThing bwt, int fb, double eP){
	//Here is the wave it refers to
		this.bosWaveThing = bwt;
	//Here are other properties deemed key for setting conditions for a general BOS wave
		this.bosWaveThing.propBOS.focusBall = fb;
		this.bosWaveThing.propBOS.endPosition = eP;
	}

	@Override
	public void setInitialCondition() {
	//This conditioner sends all points to 0 and then creates the custom wave packet
		bosWaveThing.data.setToZero();
		putPacket();
	}

	public void resetConditions(int fb, double eP){
	//for convenience, some conditioners can only reset these two components whereas others can reset all of them
		this.bosWaveThing.propBOS.focusBall = fb;
		this.bosWaveThing.propBOS.endPosition = eP;
	}

	public void resetConditions(double m, int fb, double eP, int potential, double K){
		this.bosWaveThing.propBOS.totalMass = m;
		this.bosWaveThing.propBOS.focusBall = fb;
		this.bosWaveThing.propBOS.endPosition = eP;
		this.bosWaveThing.propBOS.potential = potential;
		this.bosWaveThing.propBOS.K = K;
	}
	
	public void putPacket(){
	//basic packets, first make sure that the conditions are physically viable and then order the balls accordingly
		if(bosWaveThing.propBOS.endPosition<0.5*bosWaveThing.propBOS.focusBall/(bosWaveThing.getSpaceProps().n-1.0)){
			bosWaveThing.propBOS.endPosition = 0.5*bosWaveThing.propBOS.focusBall/(bosWaveThing.getSpaceProps().n-1.0);}
		else if(bosWaveThing.propBOS.endPosition>1.0-0.5*(bosWaveThing.getSpaceProps().n-bosWaveThing.propBOS.focusBall-1.0)/(bosWaveThing.getSpaceProps().n-1.0)){
			bosWaveThing.propBOS.endPosition=1.0-0.5*(bosWaveThing.getSpaceProps().n-bosWaveThing.propBOS.focusBall-1.0)/(bosWaveThing.getSpaceProps().n-1.0);}
		double X = bosWaveThing.propBOS.endPosition * bosWaveThing.getSpaceProps().L;
		for(int i=1;i<bosWaveThing.propBOS.focusBall;i++){
			bosWaveThing.data.x[i] = i*X/(double)bosWaveThing.propBOS.focusBall;
			bosWaveThing.data.x[i] -= i*bosWaveThing.getSpaceProps().L/(bosWaveThing.getSpaceProps().n-1.0);
			
		}
		for(int i=bosWaveThing.propBOS.focusBall;i<bosWaveThing.getSpaceProps().n-1;i++){
			bosWaveThing.data.x[i] = X+(i-bosWaveThing.propBOS.focusBall)*(bosWaveThing.getSpaceProps().L-X)/(bosWaveThing.getSpaceProps().n-bosWaveThing.propBOS.focusBall-1.0);
			bosWaveThing.data.x[i] -= i*bosWaveThing.getSpaceProps().L/(bosWaveThing.getSpaceProps().n-1.0);
		}
	}

	//Standard getters and setters for the referenced wave thing
	@Override 
	public BOSWaveThing getWaveThing(){
		return bosWaveThing;}
	@Override
	public void setWaveThing(WaveThing wt){
		try{this.bosWaveThing = (BOSWaveThing)wt;}
		catch(ClassCastException e){System.out.println("BOSInitialConditioner could not set wave");}
	}
	
	
	@Override
	public String toString(){
		return "Standard BOS conditioner";
	}
}
