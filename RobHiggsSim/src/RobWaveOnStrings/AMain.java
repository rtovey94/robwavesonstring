package RobWaveOnStrings;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.event.*;
import java.awt.*;
import java.util.ArrayList;

public class AMain {
	JFrame viewingFrame;
	JTabbedPane viewingPane;
	boolean Lesson1On = true;
	public ArrayList<WaveType> WaveTypeList = new ArrayList<WaveType>();


	public void ImportWaveTypes(){
		WaveTypeList.add(new RSFWaveType());
		WaveTypeList.add(new BallsOnSpringsWaveType());
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run(){
				new AMain();
			}
		});
	}

	public AMain(){
		viewingPane = new JTabbedPane();
		ImportWaveTypes();
		try{viewingPane.addTab("Lesson 1", new Lesson1(WaveTypeList));}catch(Exception e){System.out.println(e.toString());Lesson1On = false;}
		try{viewingPane.addTab("Lesson 2", new Lesson2(WaveTypeList));}catch(Exception e){System.out.println(e.toString());return;}
		viewingFrame = new JFrame("Waves on Strings lessons");
		viewingFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		viewingFrame.setContentPane(viewingPane);
		viewingFrame.setMinimumSize(new Dimension(750,600));
		viewingFrame.pack();
		viewingFrame.setLocationByPlatform(false);
		viewingFrame.setVisible(true);

		if(Lesson1On){
			viewingFrame.addComponentListener(new ComponentListener(){
				public void componentHidden(ComponentEvent e){}
				public void componentMoved(ComponentEvent e){}
				public void componentShown(ComponentEvent e){}
				public void componentResized(ComponentEvent e){
					((Lesson1)viewingPane.getComponentAt(0)).resetPanels();
					((Lesson2)viewingPane.getComponentAt(1)).resetPanels();
				}
			});
		}else{
			viewingFrame.addComponentListener(new ComponentListener(){
				public void componentHidden(ComponentEvent e){}
				public void componentMoved(ComponentEvent e){}
				public void componentShown(ComponentEvent e){}
				public void componentResized(ComponentEvent e){
					((Lesson2)viewingPane.getComponentAt(0)).resetPanels();
				}
			});
		}

		if(Lesson1On){
			viewingPane.addChangeListener(new ChangeListener(){
				@Override
				public void stateChanged(ChangeEvent e) {
					if(viewingPane.getSelectedIndex()==0){
						((Lesson1)viewingPane.getComponentAt(0)).myTimer.start();
						((Lesson2)viewingPane.getComponent(1)).myTimer.stop();
					}
					else{
						((Lesson1)viewingPane.getComponentAt(0)).myTimer.stop();
						((Lesson2)viewingPane.getComponent(1)).myTimer.start();
					}
				}
			});
		}

	}
}
