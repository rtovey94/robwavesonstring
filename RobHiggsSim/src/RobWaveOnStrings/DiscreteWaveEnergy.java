package RobWaveOnStrings;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.plaf.basic.BasicArrowButton;
/*
 * This displayer shows bar chart style energy density against position
 */
@SuppressWarnings("serial")
public class DiscreteWaveEnergy extends RealDataWaveDisplayer{
	private JMenuBar menuBar = new JMenuBar();
	public JMenu InputMethod,View,interactionsList;
	public JLabel Type;
	public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	private ButtonGroup group = new ButtonGroup();
	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up,down;

	
	public DiscreteWaveEnergy(RealDataWave rsfWaveThing, final OurCosmetics C){
		super();
		this.waveThing = rsfWaveThing;
		this.C = C;
		C.basicZoomFactor = Math.pow(3.5,((RealDataWave)waveThing).getSpaceProps().n/10-1)/200;
		View = new JMenu("view");
		Type = new JLabel("type");
		View.add(Type);
		InputMethod = new JMenu("Input method");
		interactionsList = new JMenu("Interactions");
	
		menuBar.add(View);
		menuBar.add(InputMethod);
		menuBar.add(interactionsList);
	
		add(menuBar);

		add(menuBar);
		menuBar.setLocation(5,5);
		menuBar.setVisible(true);
		zoomFactor = new JLabel(""+0);
		up = new BasicArrowButton(BasicArrowButton.NORTH);
		down = new BasicArrowButton(BasicArrowButton.SOUTH);
		up.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
				C.basicZoomFactor*=2;
			}
		});
		down.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
				C.basicZoomFactor/=2;
			}
		});
		add(up);
		add(down);
		add(zoomFactor);

	}

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public Color waveNumber;
		
		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

	public void addInitialConditioner(InitialConditioner IC){
		initialConditioners.add(IC);
		Methods.add(new JRadioButtonMenuItem(IC.toString()));
		group.add(Methods.get(Methods.size()-1));
		InputMethod.add(Methods.get(Methods.size()-1));
	}
	
	@Override
	public boolean setTheIC(){
		for(int j=0;j<initialConditioners.size();j++){
			if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
		}
		return true;
	}

	@Override
	protected void paintComponent(Graphics f) {
		super.paintComponent(f);
		Graphics2D g = (Graphics2D)f;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	g.setStroke(new BasicStroke(3f));
		g.drawLine(20,25, 20,getHeight()-15);
		g.drawLine(20,getHeight()-15, getWidth()-20, getHeight()-15);

		int dataLength = ((RealDataWave)waveThing).data.x.length;
		double xScale = (getWidth()-40)/(dataLength-1.0);

		g.setColor(Color.red);
		CenterLine = ((RealDataWave)waveThing).data.position/ ((RealDataWave)waveThing).getSpaceProps().delta -0.5;
		if(CenterLine>0){
			g.drawLine(20+(int)(CenterLine*xScale),25, 20+(int)(CenterLine*xScale),getHeight()-15);

		}


		double yScale = (getHeight()-40)/2.*((OurCosmetics)C).basicZoomFactor;

		Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,dataLength);
		curve.moveTo(20, getHeight()-15-((Discrete1DEnergyDensity)waveThing).energyAtIndex(0)*yScale);

		g.setColor(new Color(C.getWaveNumber().getRed(),C.getWaveNumber().getGreen(),C.getWaveNumber().getBlue(), 150));
		
		double width = (getWidth()-40.0)/(dataLength-1.);
		for(int i=0; i<dataLength; i++){
			curve.lineTo(20+i*xScale,getHeight()-15-((Discrete1DEnergyDensity)waveThing).energyAtIndex(i)*yScale);
			g.fillRect(20+(int)(i*xScale-width/2),(int)(getHeight()-15-((Discrete1DEnergyDensity)waveThing).energyAtIndex(i)*yScale), (int)width, (int)(((Discrete1DEnergyDensity)waveThing).energyAtIndex(i)*yScale));
		}

		g.setStroke(new BasicStroke(2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(C.getWaveNumber());
		g.draw(curve);

		up.setSize(15,15);
		down.setSize(15,15);
		zoomFactor.setSize(20, 15);
		down.setLocation(getWidth()-17,2);
		zoomFactor.setLocation(getWidth()-37, 2);
		up.setLocation(getWidth()-52,2);
		menuBar.setLocation(2,2);
		menuBar.setSize(menuBar.getPreferredSize());
		menuBar.doLayout();
		g.setStroke(new BasicStroke(1f));
		g.setColor(Color.black);
	}
}