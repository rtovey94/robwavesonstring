package RobWaveOnStrings;

public interface Discrete1DEnergyDensity extends DiscreteEnergyDensity {
//Must be able to calculate the energy at any point on the wave
	public double energyAtIndex(int index);
}
