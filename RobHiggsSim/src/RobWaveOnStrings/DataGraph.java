package RobWaveOnStrings;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
/*
 * This object controls the base operations of the graph in lesson 1
 */
@SuppressWarnings("serial")
public class DataGraph extends JPanel implements MouseMotionListener{
//It stores lines and the energy of that line and a boolean saying if they are shown or not
	Path2D[] Line = new Path2D.Float[20];
	private String[] dataEnergies = new String[20];
	public boolean[] showLine = new boolean[20];
//It also has a couple of objects which can display those energies
	private JLabel Energy;
	private Point Location = new Point();
	public int graphN = 0;
//And there is a button that is used by lesson 1 to let users store a wave when they want
	public JButton storeButton;

	//-----------------------------------------------------------

		public DataGraph(){
			addMouseMotionListener(this);
			Energy = new JLabel();
			setLayout(new BorderLayout());
			storeButton = new JButton("Store wave");
			add(storeButton, BorderLayout.PAGE_END);
			add(Energy);
		}

	//-----------------------------------------------------------
//If the mouse is within a couple of pixels from any shown line then it will display the energy label stored for it
//If there is more than one line that satisfies this then it will show the most recent 
		public void mouseMoved(MouseEvent e){
			Location = e.getPoint();
			Rectangle2D tmp = new Rectangle2D.Float(Location.x-1,Location.y-1, 2,2); 
			for(int i=graphN-1;i>=0;i--){
				if(showLine[i]){
					if(Line[i].intersects(tmp)){
						Energy.setText(dataEnergies[i]);
						Energy.setBackground(Color.lightGray);
						Energy.setVisible(true);
						Energy.setOpaque(true);
						repaint();
						break;
					}
					if(i==0){Energy.setVisible(false);}
				}
			}
		}
		public void mouseDragged(MouseEvent e){}
		

	//-----------------------------------------------------------

//If this is called then the line is initialised and moved to t=0 point
//If this line was already being recorded but not closed it will clear the line and start it afresh
		public void StartWave(double X){
			if(graphN<20){
				Line[graphN] = new Path2D.Float(0, 50);
				Line[graphN].moveTo(30,190-160*X);

				showLine[graphN] = true;
			}
		}

	//-----------------------------------------------------------
//This is called each time another data point is requested 
	public void addData(double X, double T){
			if (graphN<20){
				if(T<=1){
					Line[graphN].lineTo(30+T*170,190-160*X);
				}
			}
		}


	//-----------------------------------------------------------
//If the user presses store wave or it is automatically stored by lesson 1 then this is called and so it's set so that
//it stores the relevant energy and by default sets the wave to show.
		public void EndWave(String E){
			if(graphN<20){
				dataEnergies[graphN] = E;
				showLine[graphN] = true;
				graphN++;
			}
		}

	//-----------------------------------------------------------

		@Override
		protected void paintComponent(Graphics f) {
			super.paintComponent(f);
			Graphics2D g = (Graphics2D)f;

//Once a line is complete then this decides whether to draw the line or not
			for(int i=0;i<graphN;i++){
				if(showLine[i]){
					g.setStroke(getStroke(i));g.setColor(getColor(i));
				}
				g.draw(Line[i]);
			}
//If a line is being added then this will draw it 
			if((graphN<Line.length)&&(Line[graphN]!=null)){
				g.setStroke(getStroke(graphN));g.setColor(getColor(graphN));
				g.draw(Line[graphN]);
			}

//This puts the energy label beside the mouse pointer
			Energy.setSize(new Dimension(60,20));
			Energy.setLocation(Location.x,Location.y-20);
		
			g.setColor(Color.black);
			g.setStroke(new BasicStroke(3f));
			g.drawLine(30,30,30,190);
			g.drawLine(30,190,200,190);
			g.drawString("Position vs time graph", 65, 10);
		
			g.drawString("Position", 5, 25);
			g.drawString("Time", 205,195);
			g.setFont(new Font("SERIF",0,10));
			g.setStroke(new BasicStroke(1f));
			for(int i=0;i<10;i++){
				int h = (int)Math.round(i*160/10);
				g.drawLine(30,30+h,200, 30+h);
				g.drawString(""+(30-3*i),15,35+h);
			}
			g.drawString(""+0,15,195);
		
			for(int i=1;i<11;i++){
				int h = (int)Math.round(i*17);
				g.drawLine(30+h,30,30+h, 190);
				g.drawString(""+i*3,25+h,202);
			}
			g.drawString(""+0.,22,202);
		}

//Returns a straight line for the first 10 and a dashed line for the rest
	public BasicStroke getStroke(int i){
		if(i<10){return new BasicStroke(2f);}
		else{return new BasicStroke(2f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,5f,new float[]{10f}, 0f);}
	}
	
//Cycles through colours depending on i
	public Color getColor(int i){
		switch(i){
			case 0:	return Color.black;
			case 1:	return Color.blue;
			case 2:	return new Color(14,161,19);
			case 3:	return new Color(149, 16, 189);
			case 4:	return new Color(128,128,128);
			case 5:	return Color.red;
			case 6:	return Color.cyan;
			case 7:	return new Color(153,204,0);
			case 8:	return new Color(255,102,0);
			case 9:	return new Color(255,51,153);
			case 10:return Color.black;
			case 11:return Color.blue;
			case 12:return new Color(14,161,19);
			case 13:return new Color(149, 16, 189);
			case 14:return new Color(128,128,128);
			case 15:return Color.red;
			case 16:return Color.cyan;
			case 17:return new Color(153,204,0);
			case 18:return new Color(255,102,0);
			case 19:return new Color(255,51,0);
		}
		return null;
	}
}

