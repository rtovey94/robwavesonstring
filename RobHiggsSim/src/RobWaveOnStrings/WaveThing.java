package RobWaveOnStrings;

import java.util.Hashtable;
/*
 * This is the core interface that every wave must be able to implement
 * It contains methods concerning position, flattening the wave, some important getters and setters and a toString method
 */
public interface WaveThing {
//These methods give a position calculation
	public void advancePosition(double dt);
	public double getPosition();
//This method flattens the wave
	public void zeroTheWave();
//These are the getters and setters for the stored initial conditioner, current space properties and local properties
	public InitialConditioner getTheIC();
	public void setTheIC(InitialConditioner IC);
	public void setSpaceProps(SpaceProperties SP);
	public void setLocalProps(LocalProperties LP);
	public SpaceProperties getSpaceProps();
	public LocalProperties getLocalProps();
//This provides data on request for the lesson 1 data table
	public Hashtable<String,String> getLesson1DataSet();
//This method must replicate the toString method of its wave type object or it won't be recognised in the lessons
	public String typeToString();
}
