package RobWaveOnStrings;
/*
 * This class twins 2 waves in a way such that the term lambda*A*A*B*B is added to the hamiltonian
 */
public class RSFAABBInteraction implements LeapfroggableInteraction {

	RSFWaveThing A;
	RSFWaveThing B;
	double lambda;
	
	RSFAABBInteraction(
			RSFWaveThing A,
			RSFWaveThing B,
			double lambda) {
		this.A = A;
		this.B = B;
		this.lambda = lambda;
	}
	
	@Override
	public void advanceVBasedOnX(double dt) {
		
		for(int i=0; i< A.getSpaceProps().n; ++i) {
			
			A.data.v[i] -= dt * lambda
					* A.data.x[i]
					* B.data.x[i]
					* B.data.x[i];
			
			B.data.v[i] -= dt * lambda
					* B.data.x[i]
					* A.data.x[i]
					* A.data.x[i];
		
		}
	}

}
