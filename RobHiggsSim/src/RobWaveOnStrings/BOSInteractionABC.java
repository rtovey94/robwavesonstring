package RobWaveOnStrings;

public class BOSInteractionABC implements LeapfroggableInteraction{
	//This interaction adds lambda*A*B*C to the Hamiltonian

	//Stores the waves that this interaction effects and the strength of this interaction
	BOSWaveThing A,B,C;
	double lambda;
	
	public BOSInteractionABC(BOSWaveThing A, BOSWaveThing B, BOSWaveThing C, double lambda){
		this.A = A;
		this.B = B;
		this.C = C;
		this.lambda = lambda;
	}
	
//This describes the integrable component of the interaction
	@Override
	public void advanceVBasedOnX(double dt){
		for(int i=0; i< A.getSpaceProps().n; ++i) {
			A.data.v[i] -= dt * lambda
					* C.data.x[i]
					* B.data.x[i];
			B.data.v[i] -= dt * lambda
					* A.data.x[i]
					* C.data.x[i];
			C.data.v[i] -= dt * lambda
					* A.data.x[i]
					* B.data.x[i];
		}
	}

}
