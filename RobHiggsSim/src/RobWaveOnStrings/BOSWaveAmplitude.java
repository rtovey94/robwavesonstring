package RobWaveOnStrings;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.plaf.basic.BasicArrowButton;

@SuppressWarnings("serial")
public class BOSWaveAmplitude extends RealDataWaveDisplayer{
/*
 * This is a BOS wave displayer that draws balls connected by strings along a pole
 */
//Contains standard tools for displaying interactive information for the user
		private JMenuBar menuBar = new JMenuBar();
		public JMenu InputMethod,View,interactionsList;
		public JLabel Type;
		public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
		public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
		private ButtonGroup group = new ButtonGroup();
		private JLabel zoomFactor;
		private ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
		private BasicArrowButton up,down;

		public BOSWaveAmplitude(RealDataWave WaveThing, final OurCosmetics C){
			super();
			this.waveThing = WaveThing;
			this.C = C;
			View = new JMenu("view");
			Type = new JLabel("type");
			View.add(Type);
			InputMethod = new JMenu("Input method");
			interactionsList = new JMenu("Interactions");
		
			menuBar.add(View);
			menuBar.add(InputMethod);
			menuBar.add(interactionsList);
		
			add(menuBar);

			add(menuBar);
			menuBar.setLocation(5,5);
			menuBar.setVisible(true);
			
//Up/down buttons scale the zoom by factors of 2
			zoomFactor = new JLabel(""+0);
			up = new BasicArrowButton(BasicArrowButton.NORTH);
			down = new BasicArrowButton(BasicArrowButton.SOUTH);
			up.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
					C.basicZoomFactor*=2;
				}
			});
			down.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
					C.basicZoomFactor/=2;
				}
			});
			add(up);
			add(down);
			add(zoomFactor);
			zoomFactor.setLocation(getWidth()-32, 2);
			up.setLocation(getWidth()-42,2);
			up.setSize(10,10);
			down.setLocation(getWidth()-12,2);
			down.setSize(10,10);
			zoomFactor.setSize(20, 10);

			
		}
		
//Personal cosmetics contain the colour that the wave should be and the zoom factor
		static public class OurCosmetics extends GlobalCosmetics {
			public double basicZoomFactor = 1;
			public Color waveNumber;
			
			public OurCosmetics(Color c){
				if(c==null){this.waveNumber = Color.black;}
				else{this.waveNumber = c;}
			}
			
			@Override
			public Color getWaveNumber() {
				return waveNumber;
			}
		}

//This adds an initial conditioner to the menu at the top
		@Override
		public void addInitialConditioner(InitialConditioner IC){
			initialConditioners.add(IC);
			Methods.add(new JRadioButtonMenuItem(IC.toString()));
			group.add(Methods.get(Methods.size()-1));
			InputMethod.add(Methods.get(Methods.size()-1));
		}
		
//This tells the wave to take the selected conditioner as its current one
//Returns true if it is a conditioner that needs to be called and false if it does not
		@Override
		public boolean setTheIC(){
			for(int j=0;j<initialConditioners.size();j++){
				if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
			}
			return true;
		}

		@Override
		protected void paintComponent(Graphics f) {
			super.paintComponent(f);
			Graphics2D g = (Graphics2D)f;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			g.setStroke(new BasicStroke(3f));
			g.drawLine(20,25, 20,getHeight()-15);
			g.drawLine(20,getHeight()/2+5, getWidth()-20, getHeight()/2+5);


			int dataLength = ((RealDataWave)waveThing).data.x.length;
			double xScale;
			if((dataLength%2)==0){xScale = (getWidth()-40)/(dataLength-1.0);}
			else{xScale = (getWidth()-40)/(dataLength-0.0);}

			g.setColor(Color.red);
			CenterLine = ((RealDataWave)waveThing).data.position/ ((RealDataWave)waveThing).getSpaceProps().delta -0.5;
			if(CenterLine>0){
				g.drawLine(20+(int)(CenterLine*xScale),25, 20+(int)(CenterLine*xScale),getHeight()-15);

			}


			Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,dataLength);
			double yBase = getHeight()/2.+5;
			curve.moveTo(20, yBase);

			double width = (getWidth()-40.0)/(dataLength-1.)/2.;
			int height = (int) (width/2.);
			double lastX = 20+((RealDataWave)waveThing).data.x[0]/((RealDataWave)waveThing).getSpaceProps().L*(getWidth()-40);
			g.setColor(C.getWaveNumber());
			g.fillOval((int)(lastX-width/2.),(int)(yBase-width/2.), (int)width, (int)width);
			for(int i=1; i<dataLength; i++){
				double X = 20+((RealDataWave)waveThing).data.x[i]/((RealDataWave)waveThing).getSpaceProps().L*(getWidth()-40)+i*xScale;
				double delta = (X-lastX - width)/12.0;
				g.fillOval((int)(X-width/2.),(int)(yBase-width/2.), (int)width, (int)width);
				if(delta <=0){curve.lineTo(X, yBase);lastX = X;continue;}
				curve.lineTo(lastX+delta+width/2.,yBase);
				for(int j=1;j<6;j++){
					double X1 = width/2.+(4*j-1.0)*delta/2.0;
					double Y1 = height;
					double X2 = width/2.+2.*j*delta;
					double Y2 = 0;
					curve.quadTo(lastX+X1, yBase+Y1,lastX+X2, yBase+Y2);
					X1 = width/2.+(4*j+1.0)*delta/2.0;
					Y1 = -height;
					X2 = width/2.+(2*j+1.0)*delta;
					Y2 = 0;
					curve.quadTo(lastX+X1, yBase+Y1,lastX+X2, yBase+Y2);
				}
				curve.lineTo(lastX+width/2.+11*delta, yBase);
				curve.lineTo(X, yBase);
				lastX = X;
			}


			g.setStroke(new BasicStroke(1f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
			g.setColor(C.getWaveNumber());
			g.draw(curve);

//Necessary as layout manager is set null 
			up.setSize(15,15);
			down.setSize(15,15);
			zoomFactor.setSize(20, 15);
			down.setLocation(getWidth()-17,2);
			zoomFactor.setLocation(getWidth()-37, 2);
			up.setLocation(getWidth()-52,2);
			menuBar.setLocation(2,2);
			menuBar.setSize(menuBar.getPreferredSize());
			menuBar.doLayout();
			g.setStroke(new BasicStroke(1f));
			g.setColor(Color.black);
		}
}
