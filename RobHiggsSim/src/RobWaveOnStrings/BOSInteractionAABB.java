package RobWaveOnStrings;

public class BOSInteractionAABB implements LeapfroggableInteraction{
	//This interaction adds 0.5*lambda*A*A*B*B to the hamiltonian
	
//Stores the waves that this interaction effects and the strength of this interaction
	BOSWaveThing A,B;
	double lambda;
	
	public BOSInteractionAABB(BOSWaveThing A, BOSWaveThing B, double lambda){
		this.A = A;
		this.B = B;
		this.lambda = lambda;
	}
	
//This describes the integrable component of the interaction
	@Override
	public void advanceVBasedOnX(double dt){
		for(int i=0; i< A.getSpaceProps().n; ++i) {
			
			A.data.v[i] -= dt * lambda
					* A.data.x[i]
					* B.data.x[i]
					* B.data.x[i];
			
			B.data.v[i] -= dt * lambda
					* B.data.x[i]
					* A.data.x[i]
					* A.data.x[i];
		
		}
	}

}
