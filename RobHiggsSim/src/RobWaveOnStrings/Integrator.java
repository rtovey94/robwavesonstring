package RobWaveOnStrings;

import java.util.HashSet;
import java.util.Set;
/*
 * These properties are as yet uncertain, eventually a global integrator will store integratable packages
 * For now an integrator is something that can advance a set of things by a time step
 */
public abstract class Integrator {
//Should have some storage for relevant things and methods for adding and taking away
	Set<IntegratableThing> waveThings = new HashSet<IntegratableThing>();
	Set<IntegratableInteraction> interactions = new HashSet<IntegratableInteraction>();
	public abstract void add(WaveThing waveThing);
	public abstract void add(IntegratableInteraction Interaction);
	public abstract void remove(WaveThing waveThing);
	public abstract void remove(IntegratableInteraction Interaction);
//Should also advance all its stored elements by dt
	public abstract void advanceAll(double dt);
//In case future users can select integration method this will be usefull
	public abstract String toString();
}
