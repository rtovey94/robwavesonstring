package RobWaveOnStrings;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Path2D;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.plaf.basic.BasicArrowButton;
/*
 * This display shows amplitude as a string of quadratics protruding from an oval
 */
@SuppressWarnings("serial")
public class StringWaveAmplitude3D extends RealDataWaveDisplayer{
	public JMenuBar menuBar = new JMenuBar();
	public JMenu InputMethod,View,interactionsList;
	public JLabel Type;
	public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	ButtonGroup group = new ButtonGroup();
	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up;
	private BasicArrowButton down;
	
	public StringWaveAmplitude3D(RealDataWave rsfWaveThing, final OurCosmetics C){
			super();
			this.waveThing = rsfWaveThing;
			this.C = C;
			View = new JMenu("view");
			Type = new JLabel("type");
			View.add(Type);
			InputMethod = new JMenu("Input method");
			interactionsList = new JMenu("Interactions");
		
			menuBar.add(View);
			menuBar.add(InputMethod);
			menuBar.add(interactionsList);
		
			add(menuBar);

			add(menuBar);
			menuBar.setLocation(5,5);
			menuBar.setVisible(true);
			
			InputMethod.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					setTheIC();
				}
			});
			
			zoomFactor = new JLabel(""+0);
			up = new BasicArrowButton(BasicArrowButton.NORTH);
			down = new BasicArrowButton(BasicArrowButton.SOUTH);
			up.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
					C.basicZoomFactor*=2;
				}
			});
			down.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
					C.basicZoomFactor/=2;
				}
			});
			add(up);
			add(down);
			add(zoomFactor);
			zoomFactor.setLocation(getWidth()-32, 2);
			up.setLocation(getWidth()-42,2);
			up.setSize(10,10);
			down.setLocation(getWidth()-12,2);
			down.setSize(10,10);
			zoomFactor.setSize(20, 10);

	}

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public double specialZoomFactor;
		public boolean doSpecialZoomAboutPositiveMin = false;
		public Color waveNumber;

		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
		public void turnOnSpecialZoomAboutPositiveMin(double factor) {
			doSpecialZoomAboutPositiveMin = true;
			specialZoomFactor = factor;
		}

		public void turnOffSpecialZoomAboutPositiveMin() {
			doSpecialZoomAboutPositiveMin = false;
		}

		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

	@Override
	public void addInitialConditioner(InitialConditioner IC){
		initialConditioners.add(IC);
		Methods.add(new JRadioButtonMenuItem(IC.toString()));
		group.add(Methods.get(Methods.size()-1));
		InputMethod.add(Methods.get(Methods.size()-1));
	}
	
	@Override
	public boolean setTheIC(){
		for(int j=0;j<initialConditioners.size();j++){
			if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
		}
		return true;
	}

	@Override
	protected void paintComponent(Graphics f) {
		super.paintComponent(f);
		Graphics2D g = (Graphics2D)f;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		double W = (double)getWidth(), H = (double)getHeight();
		final double cosmeticScaling = 4;
		
		boolean hasVev = true;
		double vev = 0,vevVisualOffset=0;
		try {
			vev = ((RSFWaveThing)waveThing).propRSF.nonNegVev();
			if (vev != 0) {
				vevVisualOffset = cosmeticScaling * vev;
			}
		} catch (Exception e) {
			hasVev = false;
		}

		Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,((RealDataWave)waveThing).getSpaceProps().n);
		curve.moveTo(3.*W/4.0, H/2.0);
		double deltaAngle = 2 * Math.PI  / ((double) (((RealDataWave)waveThing).getSpaceProps().n));
				
		for (int j = 0; j < ((RealDataWave)waveThing).getSpaceProps().n/2; ++j) {
			double xBase1;
			double yBase1;
			double xBase2;
			double yBase2;

			final double angle = 2 * Math.PI * ((double)2*j)/ ((double) (((RealDataWave)waveThing).getSpaceProps().n));
			xBase1 = W / 2.0 + W / 4. * Math.cos(angle);
			yBase1 = H / 2.0 - H / 4. * Math.sin(angle);
			xBase2 = W / 2.0 + W / 4. * Math.cos(angle+deltaAngle);
			yBase2 = H / 2.0 - H / 4. * Math.sin(angle+deltaAngle);
		
			if (hasVev) {
				// double vevOrZero = propRSF.nonNegVevOrZero();
				yBase1 -= ((OurCosmetics)C).basicZoomFactor* vev + ((OurCosmetics)C).basicZoomFactor
						 * (((RealDataWave)waveThing).data.x[2*j] - vev);
				yBase2 -= ((OurCosmetics)C).basicZoomFactor* vev + ((OurCosmetics)C).basicZoomFactor
					 * (((RealDataWave)waveThing).data.x[2*j+1] - vev);
			} else {
				yBase1 -= ((OurCosmetics)C).basicZoomFactor* ((RealDataWave)waveThing).data.x[2*j];
				yBase2 -= ((OurCosmetics)C).basicZoomFactor* ((RealDataWave)waveThing).data.x[2*j+1];
			}

			if (vev != 0) {
			// TODO THESE SHOULD BE WITHOUT THE +-s ABOVE!
				g.setColor(Color.orange);
				g.fillRect((int) xBase1, (int) (yBase1 + vevVisualOffset), 1, 1);
				g.fillRect((int) xBase1, (int) (yBase1 - vevVisualOffset), 1, 1);
				g.fillRect((int) xBase2, (int) (yBase2 + vevVisualOffset), 1, 1);
				g.fillRect((int) xBase2, (int) (yBase2 - vevVisualOffset), 1, 1);

			}
			curve.quadTo(xBase1, yBase1, xBase2, yBase2);
		}
		curve.closePath();
		
		g.setStroke(new BasicStroke(2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(C.getWaveNumber());
		g.draw(curve);

		up.setSize(15,15);
		down.setSize(15,15);
		zoomFactor.setSize(20, 15);
		down.setLocation(getWidth()-17,2);
		zoomFactor.setLocation(getWidth()-37, 2);
		up.setLocation(getWidth()-52,2);
		menuBar.setLocation(2,2);
		menuBar.setSize(menuBar.getPreferredSize());
		menuBar.doLayout();
		g.setStroke(new BasicStroke(1f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
		g.setColor(Color.black);
		g.drawOval((int)W/4, (int)H/4, (int)W/2, (int)H/2);
		
	}
}
