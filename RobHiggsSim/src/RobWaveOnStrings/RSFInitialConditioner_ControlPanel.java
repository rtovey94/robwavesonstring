package RobWaveOnStrings;
/*
 * This class is used to set initial conditions for a basic RSF wave
 * The first slider sets mSq
 * The second slider sets pCentral
 * The third slider sets centralX
 * The forth slider sets pSpread
 */
public class RSFInitialConditioner_ControlPanel extends RSFInitialConditioner_PacketType{
private ControlPanel CP;

	public RSFInitialConditioner_ControlPanel(ControlPanel cp, RSFWaveThing rsfWaveThing){
		super(rsfWaveThing, 0, 0, 0, 1);
		this.CP = cp;
	}
	
	public void resetWave(){
		rsfWaveThing.propRSF.mSq = 0.5*(double)CP.parameterScrolls.get(0).getValue();
		rsfWaveThing.propRSF.pCentral = 0.1*((double)CP.parameterScrolls.get(1).getValue());
		rsfWaveThing.propRSF.pSpread = 0.04*((double)CP.parameterScrolls.get(3).getValue());
		rsfWaveThing.propRSF.centralX = rsfWaveThing.getSpaceProps().L/120.*((double)CP.parameterScrolls.get(2).getValue());
		
	}
	
	@Override
	public void setInitialCondition(){
		rsfWaveThing.data.setToZero();
		resetWave();
		putPacket(rsfWaveThing,rsfWaveThing.propRSF.pCentral,rsfWaveThing.propRSF.pSpread,rsfWaveThing.propRSF.centralX, 1);
	}
	
}
