package RobWaveOnStrings;
/*
 * This class shows one of the many ways that the 3 main components of putPacket can be gathered from a wave displayer
 * Position is given by initial click
 * Momentum is an exponential drag in x
 * Spread is a scalar drag in y
 */
public class RSFInitialConditioner_PacketTypeWithCatapultXandStretchY extends RSFInitialConditioner_PacketType{
	private RealDataWaveDisplayer rsfWaveDisplayer;
	
		public RSFInitialConditioner_PacketTypeWithCatapultXandStretchY(RealDataWaveDisplayer WD, RSFWaveThing WT){
			super(WT,0,0,0,1);
			this.rsfWaveDisplayer = WD;
		}
		
		public void resetWave(){
			rsfWaveThing.propRSF.pCentral = 4*(rsfWaveDisplayer.Curr.getX()-rsfWaveDisplayer.Init.getX());
			rsfWaveThing.propRSF.pSpread = Math.exp(rsfWaveDisplayer.Curr.getY()-rsfWaveDisplayer.Init.getY());
			rsfWaveThing.propRSF.centralX = rsfWaveThing.getSpaceProps().L*rsfWaveDisplayer.Init.getX();
		}
		
		@Override
		public void setInitialCondition() {
			rsfWaveThing.data.setToZero();
			resetWave();
			putPacket(rsfWaveThing,rsfWaveThing.propRSF.pCentral,rsfWaveThing.propRSF.pSpread,rsfWaveThing.propRSF.centralX,1);

		}
	

}
