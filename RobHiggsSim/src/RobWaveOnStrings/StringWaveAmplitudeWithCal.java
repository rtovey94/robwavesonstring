package RobWaveOnStrings;
/*
 * This display acts as a combination of the amplitude displayer and the amplitude displayer
 * Quadratic curves are predicted between each 2 data points to give a smooth line
 */
import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.*;
import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class StringWaveAmplitudeWithCal extends RealDataWaveDisplayer implements CalorimeterDisplayer{
	public boolean newWave,recordingWave = false;
	public double EnergyLevel;
	public double[] PX;
	public double[] PY;
	public double[] VX;
	public double[] VY;
	public double totalVSq = 0;
	
	public JMenuBar menuBar = new JMenuBar();
	public JMenu InputMethod,View,interactionsList;
	public JLabel Type;
	public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	ButtonGroup group = new ButtonGroup();
	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up,down;
	public JToggleButton hideWave = new JToggleButton();


	public StringWaveAmplitudeWithCal(RealDataWave rsfWaveThing, final OurCosmetics C){	
		super();
		this.waveThing = rsfWaveThing;
		this.C = C;
		View = new JMenu("view");
		Type = new JLabel("type");
		View.add(Type);
		InputMethod = new JMenu("Input method");
		interactionsList = new JMenu("Interactions");
		

		PX = new double[25];
		PY = new double[25];
		for(int i=0;i<25;i++){PX[i] = (10*Math.random());PY[i] = (10*Math.random());}
		VX = new double[25];
		VY = new double[25];
		double rand = 2*Math.PI*Math.random();VX[0] = Math.cos(rand);VY[0] = Math.sin(rand);
		rand = 2*Math.PI*Math.random();VX[1] = (1.3*Math.cos(rand));VY[1] = (1.3*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[2] = (1.6*Math.cos(rand));VY[2] = (1.6*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[3] = (1.9*Math.cos(rand));VY[3] = (1.9*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[4] = (1.9*Math.cos(rand));VY[4] = (1.9*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[5] = (2.2*Math.cos(rand));VY[5] = (2.2*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[6] = (2.2*Math.cos(rand));VY[6] = (2.2*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[7] = (2.5*Math.cos(rand));VY[7] = (2.5*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[8] = (2.5*Math.cos(rand));VY[8] = (2.5*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[9] = (2.5*Math.cos(rand));VY[9] = (2.5*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[10] = (2.8*Math.cos(rand));VY[10] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[11] = (2.8*Math.cos(rand));VY[11] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[12] = (2.8*Math.cos(rand));VY[12] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[13] = (2.8*Math.cos(rand));VY[13] = (2.8*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[14] = (3.1*Math.cos(rand));VY[14] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[15] = (3.1*Math.cos(rand));VY[15] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[16] = (3.1*Math.cos(rand));VY[16] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[17] = (3.1*Math.cos(rand));VY[17] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[18] = (3.1*Math.cos(rand));VY[18] = (3.1*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[19] = (3.4*Math.cos(rand));VY[19] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[20] = (3.4*Math.cos(rand));VY[20] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[21] = (3.4*Math.cos(rand));VY[21] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[22] = (3.4*Math.cos(rand));VY[22] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[23] = (3.4*Math.cos(rand));VY[23] = (3.4*Math.sin(rand));
		rand = 2*Math.PI*Math.random();VX[24] = (3.4*Math.cos(rand));VY[24] = (3.4*Math.sin(rand));
		for(int i=0;i<25;i++){totalVSq+=VX[i]*VX[i]+VY[i]*VY[i];}
		for(int i=0;i<25;i++){VX[i]*= 0.5;VY[i]*=0.5;}

		menuBar.add(View);
		menuBar.add(InputMethod);
		menuBar.add(interactionsList);
		
		hideWave.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				waveThing.getSpaceProps().restrictedView = hideWave.isSelected();
			}
		});
		add(hideWave);
		hideWave.setSelected(true);
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {
				recordingWave = false;newWave = true;
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				newWave = true;
			}
		});

		add(menuBar);
		menuBar.setVisible(true);
		zoomFactor = new JLabel(""+0);
		up = new BasicArrowButton(BasicArrowButton.NORTH);
		down = new BasicArrowButton(BasicArrowButton.SOUTH);
		up.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
				C.basicZoomFactor*=2;
			}
		});
		down.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
				C.basicZoomFactor/=2;
			}
		});
		add(up);
		add(down);
		add(zoomFactor);
		menuBar.setLocation(2,2);
		menuBar.setSize(menuBar.getPreferredSize());
		View.setLocation(0,0);
		InputMethod.setLocation(View.getWidth(),0);
		interactionsList.setLocation(View.getWidth()+InputMethod.getWidth(), 0);
		}

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public double specialZoomFactor;
		public boolean doSpecialZoomAboutPositiveMin = false;
		public Color waveNumber;
		
		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
		public void turnOnSpecialZoomAboutPositiveMin(double factor) {
			doSpecialZoomAboutPositiveMin = true;
			specialZoomFactor = factor;
		}

		public void turnOffSpecialZoomAboutPositiveMin() {
			doSpecialZoomAboutPositiveMin = false;
		}

		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

	@Override
	public void addInitialConditioner(InitialConditioner IC){
		initialConditioners.add(IC);
		Methods.add(new JRadioButtonMenuItem(IC.toString()));
		group.add(Methods.get(Methods.size()-1));
		InputMethod.add(Methods.get(Methods.size()-1));
	}
	
	@Override
	public boolean setTheIC(){
		for(int j=0;j<initialConditioners.size();j++){
			if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
		}
		return true;
	}

	@Override
		protected void paintComponent(Graphics f) {
			super.paintComponent(f);
			Graphics2D g = (Graphics2D)f;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			
			int dataLength = waveThing.getSpaceProps().n;
			double xScale;
			if((dataLength%2)==0){xScale = (getWidth()-40)/(dataLength-1.0);}
			else{xScale = (getWidth()-40)/(dataLength-1.0);}
			double yScale = (getHeight()-40)/2.*((OurCosmetics)C).basicZoomFactor;

			Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,dataLength);
			curve.moveTo(20, getHeight()/2-((RSFWaveThing)waveThing).data.x[0]*yScale+5);
			int start = 0;if(waveThing.getSpaceProps().restrictedView){start = waveThing.getSpaceProps().n/2;}
			for(int i=1; 2*i<dataLength; i++){
				curve.quadTo(20+(2*i-1)*xScale,getHeight()/2-((RSFWaveThing)waveThing).data.x[2*i-1]*yScale+5,20+2*i*xScale,getHeight()/2-((RSFWaveThing)waveThing).data.x[2*i]*yScale+5);
			}

			
/*			Path2D curve1 = new Path2D.Float(Path2D.WIND_NON_ZERO,dataLength);
			double xBase = 20;
			double yBase = getHeight()/2.+5-((RSFWaveThing)waveThing).data.x[0]*yScale;
			double width = (getWidth()-40.0)/(dataLength-1.)/2.;
			int height = (int) (width/2.);
			double lastX = yBase;
			curve1.moveTo(xBase, lastX);
			g.setColor(C.waveNumber);
			g.fillOval(20-(int)width/2,(int) (lastX - width/2), (int)width, (int)width);
			for(int i=1; i<dataLength; i++){
				double X = getHeight()/2.+5-((RealDataXVEWave)waveThing).data.x[i]*yScale;
				double delta = (xScale - width)/12.0;
				if(delta <=0){curve1.lineTo(xBase, X);continue;}
				double theta = Math.atan((X-lastX)/(xScale-width));
				double C = Math.cos(theta);
				double S = Math.sin(theta);
				xBase+=delta+width/2.;
				yBase+=(delta+width/2.)*(X-lastX)/xScale;
				curve1.lineTo(xBase,yBase);
				for(int j=0;j<5;j++){
					xBase+=delta;
					yBase+=delta*(X-lastX)/xScale;
					double Y1 = height;
					curve1.quadTo(xBase-C*delta/2 - S*Y1, yBase-S*delta/2+C*Y1,xBase, yBase);
					xBase+=delta;
					yBase+=delta*(X-lastX)/xScale;
					Y1 = -height;
					curve1.quadTo(xBase-C*delta/2 -S*Y1, yBase-S*delta/2+C*Y1,xBase, yBase);
				}
				xBase+=delta/2.0;
				yBase+=delta*(X-lastX)/xScale/2.0;
				xBase+=delta;
				yBase+=delta*(X-lastX)/xScale;
				curve1.lineTo(xBase, yBase);
				xBase+=width/2.0;
				yBase+=width/2.0*(X-lastX)/xScale;
				curve1.lineTo(xBase, X);
				xBase=20 + i*xScale;
				g.fillOval((int)(xBase-width/2.),(int)(X-width/2.), (int)width, (int)width);
				lastX = X;
			}
			g.setStroke(new BasicStroke(2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
			g.setColor(Color.red);
			g.draw(curve1);
			g.setColor(Color.black);
			
*/			
			
			
			
			
			g.setStroke(new BasicStroke(2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
			g.setColor(C.getWaveNumber());
			
			g.draw(curve);

			g.setColor(Color.black);
			Rectangle Calorimeter;
			hideWave.setSize(10, 10);
			hideWave.setLocation(getWidth()-12,getHeight()-12);

			if(!hideWave.isSelected()){
				Calorimeter = new Rectangle(getWidth()-20 - (int)(waveThing.getSpaceProps().n/4*xScale),23,21,getHeight()-35+2);
				g.setColor(new Color(C.getWaveNumber().getRed(),C.getWaveNumber().getGreen(),C.getWaveNumber().getBlue(), 150));
				g.fillRect(20,20,2*(int)(waveThing.getSpaceProps().n/4*xScale),getHeight()-20);
				g.fillRect(Calorimeter.x+Calorimeter.width,20,(int)(waveThing.getSpaceProps().n/4*xScale),getHeight()-20);

			}
			else{Calorimeter = new Rectangle(getWidth()-100-1,24-1,80+2,getHeight()-35+2);}
			for(int i=0;i<25;i++){
				g.fillOval(getWidth()-100+(int)(PX[i]*8.)-2, 24 + (int)(PY[i]*(getHeight()-35)/10)-2, 4, 4);
			}
			g.setStroke(new BasicStroke(3f));
			g.setColor(Color.black);
			g.draw(Calorimeter);
			g.drawLine(20,25, 20,getHeight()-15);
			g.drawLine(20,getHeight()/2+5, getWidth()-20, getHeight()/2+5);
			g.setColor(Color.red);
			CenterLine = ((RealDataWave)waveThing).data.position/ ((RealDataWave)waveThing).getSpaceProps().delta -0.5;
			if(CenterLine>0){
				g.drawLine(20+(int)(CenterLine*xScale),25, 20+(int)(CenterLine*xScale),getHeight()-15);
			}

			g.setFont(new Font("SERIF",0,20));
			g.setStroke(new BasicStroke(1f));
//			g.drawString("Line Type here",10,20);

//debugging line to see what is being input into putPacket
//			g.drawString("PutPacket( "+String.format("%3.2f",4*(Curr.getX()-Init.getX()))+" , "+String.format("%3.2f",Math.exp(Curr.getY()-Init.getY()))+" , "+String.format("%3.2f",data.length*Init.getX())+" , "+String.format("%3.2f",1.)+" )",10,20);

			up.setSize(15,15);
			down.setSize(15,15);
			zoomFactor.setSize(20, 15);
			down.setLocation(getWidth()-17,2);
			zoomFactor.setLocation(getWidth()-37, 2);
			up.setLocation(getWidth()-52,2);
			menuBar.setLocation(2,2);
			menuBar.setSize(menuBar.getPreferredSize());
			menuBar.doLayout();

			EnergyLevel = 0;
			for(int i=((RealDataWave)waveThing).getSpaceProps().n-1;20+(i-start)*xScale>Calorimeter.x;i--){
				EnergyLevel+=((Discrete1DEnergyDensity)waveThing).energyAtIndex(i);
			}
			if(!(EnergyLevel>0)){EnergyLevel = 0;}

			g.setStroke(new BasicStroke(1f));
		}

		@Override
		public boolean isNewWave() {return newWave;}
		@Override
		public boolean isRecordingWave() {return recordingWave;}
		@Override
		public double getEnergyLevel() {return EnergyLevel;}
		@Override
		public void setNewWave(boolean b) {newWave = b;}
		@Override
		public void setRecordingWave(boolean b) {recordingWave = b;}

		@Override
		public void progressParticles(double dt, double MaxE) {
			double tmp = Math.sqrt(EnergyLevel/MaxE*totalVSq);
			
			for(int i=0;i<25;i++){
				PX[i]+=(dt*tmp*VX[i]);PY[i]+=(dt*tmp*VY[i]);
				if(PX[i]>10){VX[i] = -Math.abs(VX[i]);PX[i] += 2*(10-PX[i]);}
				else if(PX[i]<0){VX[i] = Math.abs(VX[i]);PX[i] *= -1;}
				if(PY[i]>10){VY[i] = -Math.abs(VY[i]);PY[i] += 2*(10-PY[i]);}
				else if(PY[i]<0){VY[i] = Math.abs(VY[i]);PY[i] *= -1;}
			}
		}

}