package RobWaveOnStrings;
/*
 * This class is only used in the data table of Lesson 1
 * It has storage to retain information necessary to reproduce any given wave, hopefully just as it was 
 */
import javax.swing.JButton;

@SuppressWarnings("serial")
public class ResetButton extends JButton{
String waveType;
SpaceProperties propSpace;
LocalProperties localProps;
InitialConditioner theIC;

//-----------------------------------------------------------

	public ResetButton(Object[] Val){
		setOpaque(true);
		setText("Reset Button");
		waveType = (String) Val[0];
		propSpace = (SpaceProperties) Val[1];
		localProps = (LocalProperties) Val[2];
		theIC = (InitialConditioner) Val[3];
	}

}
