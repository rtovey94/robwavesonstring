package RobWaveOnStrings;
/*
 * Anything that can change the actual values held by a wave must implement this
 * Every wave stores its last used initial conditioner
 */

public interface InitialConditioner {
	//Every conditioner must change values for a particular wave, these guarantee that it can keep reference to that wave
	//If a conditioner needs two waves then it can extend this to reflect that
	public WaveThing getWaveThing();
	public void setWaveThing(WaveThing wt);
	
	//Must know how to set itself as its wave's current conditioner
	public void setInitialCondition();
	
	//This is what the user will see on a list when they are given choice to select one so it must be short and concise
	public String toString();
}
