package RobWaveOnStrings;

public class BallsOnSpringsWaveType extends WaveType {
/*
 * Here is the WaveType class of Balls on Springs, other than here it will always be abbreviated to BOS
 * This set of classes is designed to model the system defined by a 1D system of balls connected by simple Hooke's springs
 * The computation is predicted through the Lagrangian of such a system
 */
	public BallsOnSpringsWaveType(){
		super();
		this.inputDisplays = new BOSInterfaceList();
	}
	
	@Override
	public BOSWaveThing createNewWave(SpaceProperties propSpace,
			LocalProperties localProp) {
		//Every wave has fixed ends and is non-circular
			propSpace.setCircular(false);
		//Hopefully this is only ever initiated with correct local properties but if not something 
		// will soon throw a null pointer exception
			BOSProperties bosp;
			try{bosp = (BOSProperties)localProp;}catch(Exception e){
				System.out.println("createNewWave() has returned null in BOSWave");
				return null;}
		return new BOSWaveThing(propSpace, bosp);
	}

//The user will always see this type of wave referred to as "Balls on Springs"
	@Override
	public String toString() {
		return "Balls on Springs";
	}

//Here is the default type of local properties every BOS wave will need
	@Override
	public BOSProperties LocalProps(){
		return new BOSProperties();
	}

}
