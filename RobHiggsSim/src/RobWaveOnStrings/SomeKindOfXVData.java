package RobWaveOnStrings;
/*
 * This extension of data says that the main components will be X and V
 */
public interface SomeKindOfXVData extends SomeKindOfData {
	public void setXToZero();
	public void setVToZero();
}
