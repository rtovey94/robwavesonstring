package RobWaveOnStrings;
/*
 * This display shows the amplitude of a wave with joined sections as quadratics to make a smooth line
 */
import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.*;
import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class StringWaveAmplitude extends RealDataWaveDisplayer{
	public boolean newWave,resetWave,recordingWave = false;
	public double EnergyLevel;
	public JMenuBar menuBar = new JMenuBar();
	public JMenu InputMethod,View,interactionsList;
	public JLabel Type;
	public ArrayList<JMenu> Interactions = new ArrayList<JMenu>(5);
	public ArrayList<JRadioButtonMenuItem> Methods = new ArrayList<JRadioButtonMenuItem>();
	ButtonGroup group = new ButtonGroup();
	ArrayList<InitialConditioner> initialConditioners = new ArrayList<InitialConditioner>();
	private JLabel zoomFactor;
	private BasicArrowButton up;
	private BasicArrowButton down;


	public StringWaveAmplitude(RealDataWave RealDataWave, final OurCosmetics C) 
	/*throws Exception{
		if(RealDataWave == null){
			throw new Exception(
				"RealDataWave null in StringWaveAmplitude constructor");
		}
		if(C == null){
			throw new Exception(
				"cosmetics null in StringWaveAmplitude constructor");
		}
//	*/{	
		super();
		this.waveThing = RealDataWave;
		this.C = C;
		View = new JMenu("view");
		Type = new JLabel("type");
		View.add(Type);
		InputMethod = new JMenu("Input method");
		interactionsList = new JMenu("Interactions");
	
		menuBar.add(View);
		menuBar.add(InputMethod);
		menuBar.add(interactionsList);
	

		add(menuBar);
		menuBar.setLocation(5,5);
		menuBar.setVisible(true);
		zoomFactor = new JLabel(""+0);
		up = new BasicArrowButton(BasicArrowButton.NORTH);
		down = new BasicArrowButton(BasicArrowButton.SOUTH);
		up.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())+1));
				C.basicZoomFactor*=2;
			}
		});
		down.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				zoomFactor.setText(""+(Integer.parseInt(zoomFactor.getText())-1));
				C.basicZoomFactor/=2;
			}
		});
		add(up);
		add(down);
		add(zoomFactor);
		zoomFactor.setLocation(getWidth()-32, 2);
		up.setLocation(getWidth()-42,2);
		up.setSize(10,10);
		down.setLocation(getWidth()-12,2);
		down.setSize(10,10);
		zoomFactor.setSize(20, 10);
	
		}

	static public class OurCosmetics extends GlobalCosmetics {
		public double basicZoomFactor = 1;
		public double specialZoomFactor;
		public boolean doSpecialZoomAboutPositiveMin = false;
		public Color waveNumber;
		
		public OurCosmetics(Color c){
			if(c==null){this.waveNumber = Color.black;}
			else{this.waveNumber = c;}
		}
		
	public void turnOnSpecialZoomAboutPositiveMin(double factor) {
			doSpecialZoomAboutPositiveMin = true;
			specialZoomFactor = factor;
		}

		public void turnOffSpecialZoomAboutPositiveMin() {
			doSpecialZoomAboutPositiveMin = false;
		}

		@Override
		public Color getWaveNumber() {
			return waveNumber;
		}
	}

	@Override
	public void addInitialConditioner(InitialConditioner IC){
		initialConditioners.add(IC);
		Methods.add(new JRadioButtonMenuItem(IC.toString()));
		group.add(Methods.get(Methods.size()-1));
		InputMethod.add(Methods.get(Methods.size()-1));
	}
	
	@Override
	public boolean setTheIC(){
		for(int j=0;j<initialConditioners.size();j++){
			if(Methods.get(j).isSelected()){
				waveThing.setTheIC(initialConditioners.get(j));}
		}
		return true;
	}

	@Override
		protected void paintComponent(Graphics f) {
			super.paintComponent(f);
			Graphics2D g = (Graphics2D)f;
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

//is cosmetics "stretch around x" boolean just fit to space?
			int dataLength = ((RealDataWave)waveThing).data.x.length;
			double xScale;
			if((dataLength%2)==0){xScale = (getWidth()-40)/(dataLength-2.0);}
			else{xScale = (getWidth()-40)/(dataLength-1.0);}
			double yScale = (getHeight()-40)/2.*((OurCosmetics)C).basicZoomFactor;

			Path2D curve = new Path2D.Float(Path2D.WIND_NON_ZERO,dataLength);
			curve.moveTo(20, getHeight()/2-((RealDataWave)waveThing).data.x[0]*yScale+5);
			for(int i=1; 2*i<dataLength; i++){
				curve.quadTo(20+(2*i-1)*xScale,getHeight()/2-((RealDataWave)waveThing).data.x[2*i-1]*yScale+5,20+2*i*xScale,getHeight()/2-((RealDataWave)waveThing).data.x[2*i]*yScale+5);
			}

			g.setStroke(new BasicStroke(2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
			g.setColor(((OurCosmetics)C).waveNumber);
			
			g.draw(curve);

			g.setStroke(new BasicStroke(3f));
			g.setColor(Color.black);
			g.drawLine(20,25, 20,getHeight()-15);
			g.drawLine(20,getHeight()/2+5, getWidth()-20, getHeight()/2+5);
			g.setColor(Color.red);
			CenterLine = ((RealDataWave)waveThing).data.position/ ((RealDataWave)waveThing).getSpaceProps().delta -0.5;
			if(CenterLine>0){
				g.drawLine(20+(int)(CenterLine*xScale),25, 20+(int)(CenterLine*xScale),getHeight()-15);
			}

			g.setFont(new Font("SERIF",0,20));
			g.setStroke(new BasicStroke(1f));

			up.setSize(15,15);
			down.setSize(15,15);
			zoomFactor.setSize(20, 15);
			down.setLocation(getWidth()-17,2);
			zoomFactor.setLocation(getWidth()-37, 2);
			up.setLocation(getWidth()-52,2);
			menuBar.setLocation(2,2);
			menuBar.setSize(menuBar.getPreferredSize());
			menuBar.doLayout();
			
			g.setStroke(new BasicStroke(1f));
		}

}